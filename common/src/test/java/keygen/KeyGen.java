package keygen;

import com.atlassian.asap.core.SecurityProvider;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.spec.ECGenParameterSpec;

import static com.atlassian.asap.api.AlgorithmType.ECDSA;
import static com.atlassian.asap.api.AlgorithmType.RSA;

/**
 * A program that generates key pairs.
 * Use RSA keys with the RSxxx and PSxxx family of JWT Algorithms.
 * Use EC keys with the ESxxx family of algorithms.
 */
public class KeyGen {
    private static final File RSA_PRIVATE_KEY_PEM_FILE = new File("rsa-private-key.pem");
    private static final File RSA_PUBLIC_KEY_PEM_FILE = new File("rsa-public-key.pem");
    private static final File EC256_PRIVATE_KEY_PEM_FILE = new File("es256-private-key.pem");
    private static final File EC256_PUBLIC_KEY_PEM_FILE = new File("es256-public-key.pem");
    private static final File EC384_PRIVATE_KEY_PEM_FILE = new File("es384-private-key.pem");
    private static final File EC384_PUBLIC_KEY_PEM_FILE = new File("es384-public-key.pem");
    private static final File EC512_PRIVATE_KEY_PEM_FILE = new File("es512-private-key.pem");
    private static final File EC512_PUBLIC_KEY_PEM_FILE = new File("es512-public-key.pem");

    private static final int KEY_SIZE = 2048;
    private static final String EC_CURVE_256 = "P-256"; // the curve used with ES256, see JWA specification, sect 3.1
    private static final String EC_CURVE_384 = "P-384"; // the curve used with ES384, see JWA specification, sect 3.1
    private static final String EC_CURVE_512 = "P-521"; // the curve used with ES512, see JWA specification, sect 3.1

    public static void main(String[] args) throws Exception {
        final Provider securityProvider = SecurityProvider.getProvider();

        // RSA key pair
        KeyPairGenerator rsaGenerator = KeyPairGenerator.getInstance(RSA.algorithmName(), securityProvider);
        rsaGenerator.initialize(KEY_SIZE);
        KeyPair rsaKeyPair = rsaGenerator.generateKeyPair();
        writePem(rsaKeyPair.getPrivate(), RSA_PRIVATE_KEY_PEM_FILE);
        writePem(rsaKeyPair.getPublic(), RSA_PUBLIC_KEY_PEM_FILE);

        // EC key pair
        ECGenParameterSpec ecGenSpec = new ECGenParameterSpec(EC_CURVE_256);
        KeyPairGenerator ecGenerator = KeyPairGenerator.getInstance(ECDSA.algorithmName(), securityProvider);

        ecGenerator.initialize(ecGenSpec, new SecureRandom());
        KeyPair es256KeyPair = ecGenerator.generateKeyPair();
        writePem(es256KeyPair.getPrivate(), EC256_PRIVATE_KEY_PEM_FILE);
        writePem(es256KeyPair.getPublic(), EC256_PUBLIC_KEY_PEM_FILE);

        ecGenerator.initialize(new ECGenParameterSpec(EC_CURVE_384), new SecureRandom());
        KeyPair es384KeyPair = ecGenerator.generateKeyPair();
        writePem(es384KeyPair.getPrivate(), EC384_PRIVATE_KEY_PEM_FILE);
        writePem(es384KeyPair.getPublic(), EC384_PUBLIC_KEY_PEM_FILE);

        ecGenerator.initialize(new ECGenParameterSpec(EC_CURVE_512), new SecureRandom());
        KeyPair es512KeyPair = ecGenerator.generateKeyPair();
        writePem(es512KeyPair.getPrivate(), EC512_PRIVATE_KEY_PEM_FILE);
        writePem(es512KeyPair.getPublic(), EC512_PUBLIC_KEY_PEM_FILE);

    }

    private static void writePem(Object keyObject, File file) throws IOException {
        System.out.println("Writing " + file);
        try (JcaPEMWriter ecPublicKeyWriter = new JcaPEMWriter(new FileWriter(file))) {
            ecPublicKeyWriter.writeObject(keyObject);
        }
    }
}
