package com.atlassian.asap.api;

import com.atlassian.asap.core.serializer.Json;
import net.minidev.json.parser.ParseException;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import javax.json.JsonObject;
import java.time.Instant;
import java.util.Optional;

public class JwtBuilderBenchmark {
    @Benchmark
    public void buildJwt(BenchmarkState state) throws ParseException {
        JwtBuilder.newJwt()
                .issuer("issuer")
                .audience("audience")
                .subject(Optional.of("subject"))
                .algorithm(SigningAlgorithm.RS256)
                .expirationTime(Instant.now())
                .keyId("keyId")
                .customClaims(state.jsonObject)
                .build();
    }

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        private JsonObject jsonObject;

        public BenchmarkState() {
            jsonObject = Json.provider().createObjectBuilder().build();
        }
    }
}
