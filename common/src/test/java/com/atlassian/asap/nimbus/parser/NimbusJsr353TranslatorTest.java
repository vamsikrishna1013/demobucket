package com.atlassian.asap.nimbus.parser;

import net.minidev.json.parser.JSONParser;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonValue;
import java.io.StringReader;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class NimbusJsr353TranslatorTest {
    @Test
    public void shouldConvertJsonInArray() throws Exception {
        String originalJson = "{\"key\":[42,3.14,\"foobar\",true,false,null,{}]}";
        Object originalNimbus = new JSONParser(JSONParser.MODE_STRICTEST).parse(originalJson);
        JsonValue convertedJsr353 = NimbusJsr353Translator.nimbusToJsr353(originalNimbus);

        assertThat(convertedJsr353, is(Json.createReader(new StringReader(originalJson)).read()));
    }

    @Test
    public void shouldConvertJsonInObject() throws Exception {
        String originalJson = "{\"int\":4,\"double\":4.3,\"string\":\"foobar\",\"true\":true,\"false\":false,\"null\":null,\"object\":{},\"array\":[]}";
        Object originalNimbus = new JSONParser(JSONParser.MODE_STRICTEST).parse(originalJson);
        JsonValue convertedJsr353 = NimbusJsr353Translator.nimbusToJsr353(originalNimbus);

        assertThat(convertedJsr353, is(Json.createReader(new StringReader(originalJson)).read()));
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowWhenUnrepresentableNumbersArePresentInArray() throws Exception {
        String originalJson = "{\"key\":[1e29109282]}";
        Object originalNimbus = new JSONParser(JSONParser.MODE_STRICTEST).parse(originalJson);
        JsonValue convertedJsr353 = NimbusJsr353Translator.nimbusToJsr353(originalNimbus);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowWhenUnrepresentableNumbersArePresentInObject() throws Exception {
        String originalJson = "{\"refreshTimeout\":1e29109282}";
        Object originalNimbus = new JSONParser(JSONParser.MODE_STRICTEST).parse(originalJson);
        JsonValue convertedJsr353 = NimbusJsr353Translator.nimbusToJsr353(originalNimbus);
    }
}
