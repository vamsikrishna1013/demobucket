package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.InvalidHeaderException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.security.PublicKey;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChainedKeyProviderTest {
    public static final String KEY_ID = "key-1";
    public static final URI KEY_URI = URI.create("test:" + KEY_ID);

    @Mock
    private KeyProvider<PublicKey> provider1;
    @Mock
    private KeyProvider<PublicKey> provider2;
    @Mock
    private PublicKey key1;

    private ValidatedKeyId validatedKeyId;

    private ChainedKeyProvider keyProvider;

    @Before
    public void initialise() throws InvalidHeaderException {
        keyProvider = new ChainedKeyProvider(ImmutableList.of(provider1, provider2));
        validatedKeyId = ValidatedKeyId.validate(KEY_ID);
    }

    @Test
    public void shouldSuccessfullyGetKeyFromFirstProvider() throws Exception {
        when(provider1.getKey(validatedKeyId)).thenReturn(key1);
        assertThat(keyProvider.getKey(validatedKeyId), is(key1));
        verifyZeroInteractions(provider2);
    }

    @Test
    public void shouldSuccessfullyGetKeyFromSecondProviderIfFirstOneFails() throws Exception {
        when(provider1.getKey(validatedKeyId)).thenThrow(new PublicKeyRetrievalException("", validatedKeyId, KEY_URI));
        when(provider2.getKey(validatedKeyId)).thenReturn(key1);
        assertThat(keyProvider.getKey(validatedKeyId), is(key1));
        verify(provider1).getKey(validatedKeyId);
        verify(provider2).getKey(validatedKeyId);
    }

    @Test
    public void shouldFailoverIfKeyDoesNotExistInFirstProvider() throws Exception {
        when(provider1.getKey(validatedKeyId)).thenThrow(new PublicKeyNotFoundException("", validatedKeyId, KEY_URI));
        when(provider2.getKey(validatedKeyId)).thenReturn(key1);
        assertThat(keyProvider.getKey(validatedKeyId), is(key1));
        verify(provider1).getKey(validatedKeyId);
        verify(provider2).getKey(validatedKeyId);
    }

    @Test
    public void shouldThrowCannotRetrieveKeyExceptionIfAllProvidersAreDown() throws Exception {
        when(provider1.getKey(validatedKeyId)).thenThrow(new CannotRetrieveKeyException(""));
        when(provider2.getKey(validatedKeyId)).thenThrow(new CannotRetrieveKeyException(""));
        try {
            keyProvider.getKey(validatedKeyId);
            fail("Should have thrown");
        } catch (PublicKeyNotFoundException e) {
            fail("Should not throw this exception because all providers were down");
        } catch (CannotRetrieveKeyException e) {
            verify(provider1).getKey(validatedKeyId);
            verify(provider2).getKey(validatedKeyId);
        }
    }

    @Test
    public void shouldThrowCannotRetrieveKeyExceptionIfAnyProviderIsDownAndKeyCannotBeFoundInTheOthers() throws Exception {
        when(provider1.getKey(validatedKeyId)).thenThrow(new PublicKeyNotFoundException("", validatedKeyId, KEY_URI));
        when(provider2.getKey(validatedKeyId)).thenThrow(new CannotRetrieveKeyException(""));
        try {
            keyProvider.getKey(validatedKeyId);
            fail("Should have thrown");
        } catch (PublicKeyNotFoundException e) {
            fail("Should not throw this exception because there was at least one failure");
        } catch (CannotRetrieveKeyException e) {
            verify(provider1).getKey(validatedKeyId);
            verify(provider2).getKey(validatedKeyId);
        }
    }

    @Test
    public void shouldThrowPublicKeyNotFoundExceptionIfKeyIsNotFoundAnywhereButAllRepositoriesAreUp() throws Exception {
        when(provider1.getKey(validatedKeyId)).thenThrow(new PublicKeyNotFoundException("", validatedKeyId, KEY_URI));
        when(provider2.getKey(validatedKeyId)).thenThrow(new PublicKeyNotFoundException("", validatedKeyId, KEY_URI));
        try {
            keyProvider.getKey(validatedKeyId);
            fail("Should have thrown");
        } catch (PublicKeyNotFoundException e) {
            verify(provider1).getKey(validatedKeyId);
            verify(provider2).getKey(validatedKeyId);
        }
    }

    @Test
    public void shouldNotWrapForSingleProvider() {
        List<KeyProvider<PublicKey>> providers = ImmutableList.of(provider1);
        KeyProvider<PublicKey> chainedProvider = ChainedKeyProvider.createChainedKeyProvider(providers);
        assertThat(chainedProvider, sameInstance(provider1));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldFailWhenNoProviders() throws CannotRetrieveKeyException, InvalidHeaderException {
        KeyProvider<PublicKey> chainedProvider = ChainedKeyProvider.createChainedKeyProvider(ImmutableList.<KeyProvider<PublicKey>>of());
        chainedProvider.getKey(validatedKeyId);
    }
}
