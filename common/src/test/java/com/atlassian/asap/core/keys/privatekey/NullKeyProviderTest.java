package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Test;

public class NullKeyProviderTest {
    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldAlwaysFail() throws Exception {
        new NullKeyProvider().getKey(ValidatedKeyId.validate("anything"));
    }
}
