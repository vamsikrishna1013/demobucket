package com.atlassian.asap.core.keys;

import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class DataUriUtilTest {
    @Test
    public void shouldDecodeUrlEncodedKeyId() {
        String keyId = DataUriUtil.getKeyId("data:mime/type;kid=a%2Fb%2Fc%2Bd%2Fapi.key;base64,encoded/key+data==");
        assertThat(keyId, is("a/b/c+d/api.key"));
    }

    @Test
    public void shouldDecodeBase64EncodedKeyData() {
        byte[] keyData = DataUriUtil.getKeyData("data:mime/type;kid=a%2Fb%2Fc-key;base64,U2VjcmV0IEtleSE=");
        assertThat(keyData, is("Secret Key!".getBytes(StandardCharsets.US_ASCII)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectOtherSchemes() {
        byte[] keyData = DataUriUtil.getKeyData("keys:mime/type;kid=a%2Fb%2Fc-key;base64,U2VjcmV0IEtleSE=");
        assertThat(keyData, is("Secret Key!".getBytes(StandardCharsets.US_ASCII)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldHandleMissingKeyId() {
        DataUriUtil.getKeyId("data:mime/type;base64,encoded/key+data==");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldHandleMissingKeyData() {
        DataUriUtil.getKeyId("data:mime/type;kid=a%2Fb%2Fc+d-key;base64,");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectKeyIdsWithoutUrlEncoding() {
        DataUriUtil.getKeyId("data:mime/type;kid=a/b/c/key;base64,encoded/key+data==");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectKeyDataWithoutBase64Encoding() {
        DataUriUtil.getKeyData("data:mime/type;kid=a%2Fb%2Fc-key;none,secret");
    }
}
