package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.DataUriKeyReader;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.InputStreamReader;
import java.net.URI;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class DataUriKeyProviderTest {
    private static final String VALID_KID = "issuer/kid";
    private static final URI VALID_DATA_URI = URI.create("data:application/pkcs8;kid=issuer%2Fkid;base64,EncodedKeyData");

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private DataUriKeyReader keyReader;
    @Mock
    private RSAPrivateKey privateKey;

    @Test
    public void shouldBeAbleToReadKeyFromDataUri() throws Exception {
        when(keyReader.readPrivateKey(any(InputStreamReader.class))).thenReturn(privateKey);
        KeyProvider<PrivateKey> keyRetriever = new DataUriKeyProvider(VALID_DATA_URI, keyReader);

        assertSame(privateKey, keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldGetErrorWhenKeyParsingFails() throws Exception {
        when(keyReader.readPrivateKey(any(InputStreamReader.class)))
                .thenThrow(new CannotRetrieveKeyException("Random error"));
        KeyProvider<PrivateKey> keyRetriever = new DataUriKeyProvider(VALID_DATA_URI, keyReader);

        keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenDataUriIsInvalid() throws Exception {
        KeyProvider<PrivateKey> keyRetriever = new DataUriKeyProvider(URI.create("data:invalid"), keyReader);

        keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldFailWhenKeyIdIsNotDefined() throws Exception {
        KeyProvider<PrivateKey> keyRetriever = new DataUriKeyProvider(VALID_DATA_URI, keyReader);

        keyRetriever.getKey(ValidatedKeyId.validate("some/other/key/identifier"));
    }

    @Test
    public void shouldParseKeyFromDataUriOnlyOnceOnConstruction() throws Exception {
        when(keyReader.readPrivateKey(any(InputStreamReader.class))).thenReturn(privateKey);
        KeyProvider<PrivateKey> keyRetriever = new DataUriKeyProvider(VALID_DATA_URI, keyReader);
        verify(keyReader).readPrivateKey(any(InputStreamReader.class));

        assertSame(privateKey, keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID)));
        verifyNoMoreInteractions(keyReader);
    }
}
