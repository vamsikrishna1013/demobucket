package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.DataUriKeyReader;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.InputStreamReader;
import java.net.URI;
import java.security.PrivateKey;
import java.util.Optional;

import static com.atlassian.asap.core.keys.privatekey.EnvironmentVariableKeyProvider.Environment;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class EnvironmentVariableKeyProviderTest {
    private static final String VALID_KID = "issuer/kid";
    private static final String PROPERTY_NAME = "property.name";
    private static final URI VALID_URL = URI.create("env:///" + PROPERTY_NAME);
    private static final String VALID_KEY_DATA = "data:application/pkcs8;kid=issuer%2Fkid;base64,EncodedKeyData";

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private DataUriKeyReader keyReader;
    @Mock
    private Environment environment;
    @Mock
    private PrivateKey privateKey;

    @Test
    public void shouldBeAbleToReadKeyFromEnvironment() throws Exception {
        when(environment.getVariable(PROPERTY_NAME)).thenReturn(Optional.of(VALID_KEY_DATA));
        when(keyReader.readPrivateKey(any(InputStreamReader.class))).thenReturn(privateKey);
        KeyProvider<PrivateKey> keyProvider = new EnvironmentVariableKeyProvider(PROPERTY_NAME, keyReader, environment);

        assertSame(privateKey, keyProvider.getKey(ValidatedKeyId.validate(VALID_KID)));
    }

    @Test
    public void shouldFailWhenKeyIsNotAUri() throws Exception {
        when(environment.getVariable(PROPERTY_NAME)).thenReturn(Optional.of("unparseable private key data uri"));

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(not(containsString("private key")));

        KeyProvider<PrivateKey> keyProvider = new EnvironmentVariableKeyProvider(PROPERTY_NAME, keyReader, environment);
        keyProvider.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test
    public void shouldFailWhenKeyParserFails() throws Exception {
        when(environment.getVariable(PROPERTY_NAME)).thenReturn(Optional.of(VALID_KEY_DATA));
        when(keyReader.readPrivateKey(any(InputStreamReader.class)))
                .thenThrow(new CannotRetrieveKeyException("Random error"));

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(not(containsString(VALID_KEY_DATA)));

        // we don't care whether it fails on construction (eager) or on use (lazy)
        KeyProvider<PrivateKey> keyProvider = new EnvironmentVariableKeyProvider(PROPERTY_NAME, keyReader, environment);
        keyProvider.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test
    public void shouldFailWhenEnvironmentVariableIsNotDefined() throws Exception {
        when(environment.getVariable(PROPERTY_NAME)).thenReturn(Optional.empty());

        expectedException.expect(IllegalArgumentException.class);

        KeyProvider<PrivateKey> keyProvider = EnvironmentVariableKeyProvider.fromUri(VALID_URL, keyReader);
        keyProvider.getKey(ValidatedKeyId.validate("some/other/key/identifier"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToConstructIfUriDoesNotHaveEnvSchema() throws Exception {
        EnvironmentVariableKeyProvider.fromUri(new URI("env:opaque"), keyReader);
    }
}
