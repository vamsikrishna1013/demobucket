package com.atlassian.asap.core.keys.publickey;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.protocol.HttpContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class S3ServiceUnavailableRetryStrategyTest {
    @Mock
    private HttpResponse httpResponse;

    @Mock
    private HttpContext httpContext;

    @Mock
    private StatusLine statusLine;

    private S3ServiceUnavailableRetryStrategy retryStrategy;

    @Before
    public void createSut() {
        retryStrategy = new S3ServiceUnavailableRetryStrategy(2, 100);
    }

    @Test
    public void shouldRetryOn500() {
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(500);

        assertThat(retryStrategy.retryRequest(httpResponse, 1, httpContext), is(true));
    }

    @Test
    public void shouldNotRetryOn400() {
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(400);

        assertThat(retryStrategy.retryRequest(httpResponse, 1, httpContext), is(false));
    }

    @Test
    public void shouldStopRetrying() {
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(500);

        assertThat(retryStrategy.retryRequest(httpResponse, 5, httpContext), is(false));
    }

}
