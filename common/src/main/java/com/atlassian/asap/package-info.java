/**
 * Library that is used to create and verify json web tokens (JWT)
 * for service to service authentication purposes using the ASAP protocol.
 *
 * @see <a href="https://tools.ietf.org/html/rfc7519">JSON Web Token</a>
 * @see <a href="http://s2sauth.bitbucket.org/">ASAP Authentication</a>
 */
@javax.annotation.ParametersAreNonnullByDefault
@com.atlassian.asap.annotation.FieldsAreNonNullByDefault
@com.atlassian.asap.annotation.ReturnTypesAreNonNullByDefault
package com.atlassian.asap;