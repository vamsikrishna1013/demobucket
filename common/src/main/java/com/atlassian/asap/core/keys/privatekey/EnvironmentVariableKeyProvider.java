package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.core.keys.DataUriKeyReader;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Reads private keys specified as data uri from environment variables.
 *
 * <p>Note that Environment variables are case-insensitive in some systems (Windows). However, ASAP key identifiers are case-sensitive.
 * That means that the same environment variable will provide the private key for "keyidentifier", "KEYIDENTIFIER", "keyIdentifier" and so on.
 *
 * @see System#getenv(String)
 */
public final class EnvironmentVariableKeyProvider extends DataUriKeyProvider {
    static final String URL_SCHEME = "env";
    private static final Logger logger = LoggerFactory.getLogger(EnvironmentVariableKeyProvider.class);

    @VisibleForTesting
    EnvironmentVariableKeyProvider(String variableName, DataUriKeyReader keyReader, Environment environment) {
        super(getDataUriFromEnvironment(variableName, environment), keyReader);
    }

    private static URI getDataUriFromEnvironment(String variableName, Environment environment) {
        logger.debug("Reading private key from environment variable {}", variableName);
        String environmentVariableValue = environment.getVariable(variableName)
                .orElseThrow(() -> new IllegalArgumentException("Undefined environment variable: " + variableName));
        try {
            return new URI(environmentVariableValue);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Value of environment variable " + variableName +
                    " cannot be parsed as a URI");
        }
    }

    /**
     * Instantiates the class by parsing a URL with the following syntax:
     * <code>env:///[BASE_ENV_VARIABLE_NAME]</code>.
     *
     * @param uri       URI with the syntax described above
     * @param keyReader reader of PEM documents
     * @return a new instance of the class
     */
    public static EnvironmentVariableKeyProvider fromUri(URI uri, DataUriKeyReader keyReader) {
        return fromUri(uri, keyReader, new Environment());
    }

    @VisibleForTesting
    static EnvironmentVariableKeyProvider fromUri(URI uri, DataUriKeyReader keyReader, Environment environment) {
        checkArgument(uri.isAbsolute(), "URL must be absolute"); // implies that scheme != null
        checkArgument(URL_SCHEME.equals(uri.getScheme()), "URL must have " + URL_SCHEME + " scheme");
        checkArgument(uri.getPath() != null && uri.getPath().startsWith("/"), "URL must have a path component");
        checkArgument(uri.getQuery() == null, "URL must not have a query component");
        checkArgument(uri.getAuthority() == null, "URL must not have an authority component");

        String basePropertyName = uri.getPath().substring(1); // remove the trailing slash
        return new EnvironmentVariableKeyProvider(basePropertyName, keyReader, environment);
    }

    static class Environment {
        public Optional<String> getVariable(String name) {
            return Optional.ofNullable(System.getenv(name));
        }
    }
}
