package com.atlassian.asap.core.validator;

import com.atlassian.asap.core.exception.InvalidHeaderException;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Represents a validated key id of the JWT Header. A validated key id is safe to be used in the path component of a url
 * without causing path traversal.
 */
public final class ValidatedKeyId {
    /**
     * Subset of the valid characters for URI Paths derived from <a href="https://tools.ietf.org/html/rfc3986#section-3.3">RFC3986</a>.
     * The subset has been chosen to minimise security risks, including variable expansion,
     * query parameters injection and unexpected string terminators.
     */
    public static final Pattern PATH_PATTERN = Pattern.compile("^[\\w.\\-\\+/]*$");

    private static final Set<String> PATH_TRAVERSAL_COMPONENTS = ImmutableSet.of(".", "..");
    private static final Pattern PATH_SPLITTER = Pattern.compile("/");

    private static final Logger logger = LoggerFactory.getLogger(ValidatedKeyId.class);

    private final String keyId;

    private ValidatedKeyId(String validatedKeyId) {
        this.keyId = Objects.requireNonNull(validatedKeyId);
    }

    /**
     * Validates the given key id and upon successful validation returns a {@link ValidatedKeyId} instance.
     *
     * @param unvalidatedKeyId a key identifier for public or private key
     * @return a validated key id instance if the given key id passes the validation
     * @throws InvalidHeaderException if the key id is invalid or unsafe to use
     */
    public static ValidatedKeyId validate(String unvalidatedKeyId) throws InvalidHeaderException {
        if (isBlank(unvalidatedKeyId)) {
            logger.debug("Rejecting absent or blank kid");
            throw new InvalidHeaderException("The kid header is required");
        }

        // Prevent directory traversal (e.g. ../../../etc/passwd)
        List<String> pathComponents = PATH_SPLITTER.splitAsStream(unvalidatedKeyId)
                .map(StringUtils::trim)
                .collect(Collectors.toList());
        if (pathComponents.stream().anyMatch(PATH_TRAVERSAL_COMPONENTS::contains)) {
            logger.debug("Rejecting kid value {} because it contains path traversal", unvalidatedKeyId);
            throw new InvalidHeaderException("Path traversal components not allowed in kid");
        }

        // Prevent empty components (e.g. foo//bar/baz), also covers the case of leading slashes
        if (pathComponents.stream().anyMatch(StringUtils::isBlank)) {
            logger.debug("Rejecting kid value {} because it is in invalid format", unvalidatedKeyId);
            throw new InvalidHeaderException("Invalid format of kid");
        }

        // the kid must be a valid URL path fragment (e.g., no special chars, no '?')
        if (!PATH_PATTERN.matcher(unvalidatedKeyId).matches()) {
            logger.debug("Rejecting kid value {} because it contains invalid characters", unvalidatedKeyId);
            throw new InvalidHeaderException("Invalid character found in kid");
        }

        return new ValidatedKeyId(unvalidatedKeyId);
    }

    /**
     * @return a validated key id that is safe to be used in the path component of a url without causing path traversal
     */
    public String getKeyId() {
        return keyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ValidatedKeyId that = (ValidatedKeyId) o;

        return new EqualsBuilder()
                .append(keyId, that.keyId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(keyId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("keyId", keyId)
                .toString();
    }
}
