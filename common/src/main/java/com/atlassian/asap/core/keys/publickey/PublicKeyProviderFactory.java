package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URI;
import java.security.PublicKey;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * <p>Factory for creating {@link KeyProvider KeyProvider&lt;PublicKey&gt;} instances.</p>
 *
 * <p>The factory supports multiple key repositories for fail overs. The key repositories can be specified as mirrored or chained
 * in the public key base url.
 * <ul>
 *     <li>Chained key repositories must be comma (,) separated, with one or more whitespace before and after the comma.
 *         They are accessed in the order they are specified.</li>
 *     <li>Mirrored key repositories must be separated by the pipe (|) character, with one or more whitespace before and after the pipe.</li>
 *  </ul>
 * <p> The repository types can be combined, e.g. for a public key base url <code>"a , b | c , d"</code>,
 * the public key is looked-up first in a, if it fails then b or c, and then finally c. </p>
 *
 * @see ChainedKeyProvider
 * @see MirroredKeyProvider
 */
public class PublicKeyProviderFactory {
    private static final Pattern CHAIN_SPLITTER_REGEX = Pattern.compile("\\s+,\\s+");
    private static final Pattern MIRRORS_SPLITTER_REGEX = Pattern.compile("\\s+\\|\\s+");

    private static final Logger logger = LoggerFactory.getLogger(PublicKeyProviderFactory.class);

    private final PemReader pemReader;
    private final HttpClient httpClient;

    public PublicKeyProviderFactory(HttpClient httpClient, PemReader pemReader) {
        this.pemReader = Objects.requireNonNull(pemReader);
        this.httpClient = Objects.requireNonNull(httpClient);
    }

    /**
     * @return a default instance of the factory
     */
    public static PublicKeyProviderFactory createDefault() {
        return new PublicKeyProviderFactory(HttpPublicKeyProvider.defaultHttpClient(), new PemReader());
    }

    /**
     * Creates an instance of {@link com.atlassian.asap.core.keys.KeyProvider} for the given base URL.
     *
     * @param publicKeyBaseUrl the base URL of the public key server. See the class Javadoc for syntax details.
     * @return a public key provider
     * @throws IllegalArgumentException if the publicKeyBaseUrl is syntactically incorrect
     */
    public KeyProvider<PublicKey> createPublicKeyProvider(String publicKeyBaseUrl) {
        Objects.requireNonNull(publicKeyBaseUrl);
        logger.info("Using {} as public key base url", publicKeyBaseUrl);
        return parseChainedPublicKeyProviders(publicKeyBaseUrl);
    }

    private KeyProvider<PublicKey> parseChainedPublicKeyProviders(String publicKeyBaseUrl) {
        List<KeyProvider<PublicKey>> keyProviderChain = CHAIN_SPLITTER_REGEX.splitAsStream(publicKeyBaseUrl)
                .map(String::trim)
                .filter(StringUtils::isNotBlank)
                .map(this::parseMirroredPublicKeyProviders)
                .collect(Collectors.toList());

        if (keyProviderChain.isEmpty()) {
            logger.warn("No key providers available, all requests will be rejected.");
        }

        return ChainedKeyProvider.createChainedKeyProvider(keyProviderChain);
    }

    private KeyProvider<PublicKey> parseMirroredPublicKeyProviders(String baseUrlComponent) {
        List<KeyProvider<PublicKey>> mirroredProviders = MIRRORS_SPLITTER_REGEX.splitAsStream(baseUrlComponent)
                .map(String::trim)
                .filter(StringUtils::isNotBlank)
                .map(this::getPublicKeyKeyProvider)
                .collect(Collectors.toList());

        return MirroredKeyProvider.createMirroredKeyProvider(mirroredProviders);
    }

    private KeyProvider<PublicKey> getPublicKeyKeyProvider(String baseUrl) {
        URI parsedBaseUrl = URI.create(baseUrl);
        checkArgument(parsedBaseUrl.isAbsolute(), "URL must be absolute");
        checkArgument(!baseUrl.contains(","), "Comma must be encoded if present in URI");
        checkArgument(!baseUrl.contains("|"), "Pipe must be encoded if present in URI");
        switch (parsedBaseUrl.getScheme()) {
            case "https":
                return new HttpPublicKeyProvider(parsedBaseUrl, httpClient, pemReader);
            case "file":
                return new FilePublicKeyProvider(new File(parsedBaseUrl), pemReader);
            case "classpath":
                return new ClasspathPublicKeyProvider(parsedBaseUrl.getPath(), pemReader);
            default:
                throw new IllegalArgumentException("Unsupported public key server base URL protocol, " +
                        "only https:, file: and classpath: are supported: " + parsedBaseUrl);
        }
    }

}
