package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.JwsHeader;

/**
 * Thrown when a required header is missing in the JWT header.
 */
public class MissingRequiredHeaderException extends JwtParseException {
    private final JwsHeader.Header header;

    public MissingRequiredHeaderException(JwsHeader.Header missingHeader) {
        super("JWT token missing required header: " + missingHeader.key());
        this.header = missingHeader;
    }

    public JwsHeader.Header getHeader() {
        return header;
    }

    @Override
    public String getSafeDetails() {
        return super.getSafeDetails() + " - " + header;
    }
}
