package com.atlassian.asap.core.keys;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataUriUtil {
    /**
     * A regular expression pattern to match a data uri in the following format:
     * <pre>
     *  data:{key-mediatype};kid={key-identifier};base64,{encoded-key-data}
     * </pre>
     * The value of the kid parameter must be url encoded, in conformance to RFC2397.
     *
     * @see <a href="https://tools.ietf.org/html/rfc2397">RFC 2397 The "data" uri scheme</a>
     */
    private static Pattern DATA_URI_PATTERN = Pattern.compile("data:(?<mimeType>[\\w\\-]+/[\\w\\-]+);kid=(?<keyId>[\\w.\\-\\+%]*);base64," +
            "(?<encodedData>[A-Za-z0-9+/=]+)");

    /**
     * Extract the key data from the given data uri. The key data must be base64 encoded.
     *
     * @param dataUri the data uri to extract the key data from
     * @return the decoded key data
     */
    public static byte[] getKeyData(String dataUri) {
        String rawKeyData = getParameterFromDataUri(dataUri, "encodedData");
        return Base64.getDecoder().decode(rawKeyData);
    }

    /**
     * Extract the key identifier from the given data uri. The key identifier must be url encoded.
     *
     * @param dataUri the data uri to extract the key id from
     * @return the decoded key identifier
     */
    public static String getKeyId(String dataUri) {
        String rawKeyId = getParameterFromDataUri(dataUri, "keyId");
        try {
            return URLDecoder.decode(rawKeyId, StandardCharsets.US_ASCII.name());
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("Could not decode key id parameter in data uri. Key id must be url encoded.", e);
        }
    }

    private static String getParameterFromDataUri(String dataUri, String parameter) {
        Matcher dataUriMatcher = DATA_URI_PATTERN.matcher(dataUri);
        if (!dataUriMatcher.matches() || dataUriMatcher.groupCount() != 3) {
            throw new IllegalArgumentException("Unable to parse data uri parameter: " + parameter);
        }
        return dataUriMatcher.group(parameter);
    }

}
