package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.KeyReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;

import java.io.StringReader;
import java.security.PrivateKey;
import java.util.Objects;
import java.util.Optional;

/**
 * Reads a single private key from a String. The key and the key identifier are construction arguments, and the
 * provider only provides that particular key identifier. This provider can be used for private keys obtained from
 * arbitrary sources, like an application-specific configuration file. If the key is provided via a classpath resource,
 * or a file, or an environment variable, or a system property, then please use one of the specialised private key
 * providers for those scenarios.
 *
 * <p>See also {@link DataUriKeyProvider}, which can be used for a similar purpose but only when the key is provided in
 * the data URI format. {@link StringPrivateKeyProvider} also allows other formats, like PEM keys.
 */
public class StringPrivateKeyProvider implements KeyProvider<PrivateKey> {
    private final String keyId;
    private final Optional<PrivateKey> privateKey;

    /**
     * Creates an instance that provides a single key read from a String. It attempts to parse the key at construction
     * time. If the key is not parseable, it will fail at retrieval time.
     *
     * @param keyReader the key reader to parse the key
     * @param key the actual private key
     * @param keyId the key identifier
     */
    public StringPrivateKeyProvider(KeyReader keyReader, String key, String keyId) {
        this.keyId = Objects.requireNonNull(keyId);
        this.privateKey = tryReadPrivateKey(keyReader, key);  // eagerly parse the key
    }

    private static Optional<PrivateKey> tryReadPrivateKey(KeyReader keyReader, String key) {
        try {
            return Optional.of(keyReader.readPrivateKey(new StringReader(key)));
        } catch (CannotRetrieveKeyException e) {
            return Optional.empty();  // will fail later at retrieval time
        }
    }

    @Override
    public PrivateKey getKey(ValidatedKeyId keyId) throws CannotRetrieveKeyException {
        if (keyId.getKeyId().equals(this.keyId)) {
            return privateKey.orElseThrow(() -> new CannotRetrieveKeyException("Cannot parse private key"));
        } else {
            throw new CannotRetrieveKeyException("Cannot find private key");
        }
    }
}
