package com.atlassian.asap.core.keys;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.SecurityProvider;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.encoders.DecoderException;

import java.io.IOException;
import java.io.Reader;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;

/**
 * Functions for reading PEM keys.
 */
public class PemReader implements KeyReader {

    private final Provider provider;

    public PemReader() {
        this(SecurityProvider.getProvider());
    }

    public PemReader(Provider provider) {
        this.provider = provider;
    }

    /**
     * Reads a private key from a PEM file.
     *
     * @param reader source of the PEM data
     * @return the key
     * @throws CannotRetrieveKeyException if the key cannot be read, parsed, or the algorithm is unknown
     */
    public PrivateKey readPrivateKey(Reader reader) throws CannotRetrieveKeyException {
        try {
            PEMParser pemParser = new PEMParser(reader);
            Object keyPairObject = pemParser.readObject();
            if (keyPairObject instanceof PEMKeyPair) {
                PEMKeyPair pemKeyPair = (PEMKeyPair) keyPairObject;
                JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider(provider);
                return converter.getPrivateKey(pemKeyPair.getPrivateKeyInfo());
            } else {
                throw new CannotRetrieveKeyException("Error reading PEM private key, unknown key pair object type");
            }
        } catch (IOException | DecoderException e) {
            throw new CannotRetrieveKeyException("Error reading PEM private key", e);
        }
    }

    /**
     * Reads a public key from a PEM file.
     *
     * @param reader source of the PEM data
     * @return the key
     * @throws CannotRetrieveKeyException if the key cannot be read or parsed
     */
    public PublicKey readPublicKey(Reader reader) throws CannotRetrieveKeyException {
        try {
            PEMParser pemParser = new PEMParser(reader);
            Object object = pemParser.readObject();

            SubjectPublicKeyInfo pub = SubjectPublicKeyInfo.getInstance(object);
            JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider(provider);

            return converter.getPublicKey(pub);
        } catch (IOException | DecoderException e) {
            throw new CannotRetrieveKeyException("Error reading PEM public key", e);
        }
    }
}
