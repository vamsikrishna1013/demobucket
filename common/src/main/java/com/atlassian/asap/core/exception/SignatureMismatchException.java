package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.exception.InvalidTokenException;

/**
 * Indicates that the token signature does not match its contents.
 */
public class SignatureMismatchException extends InvalidTokenException {
    public SignatureMismatchException(Throwable cause) {
        super(cause);
    }

    public SignatureMismatchException(String reason) {
        super(reason);
    }
}
