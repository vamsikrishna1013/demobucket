package com.atlassian.asap.core.keys;

import javax.annotation.Nullable;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * A class to create <strong>informative</strong> classpath URI.
 */
public final class ClassPathUri {
    /**
     * Creates a URI that denotes a classpath resource, using the <em>classpath</em> pseudo protocol.
     *
     * @param path the path to the resource in the classpath
     * @return a URI that denotes a classpath resource or {@code null} if there was a syntax exception
     */
    @Nullable
    public static URI classPathUri(String path) {
        try {
            return new URI("classpath:" + path);
        } catch (URISyntaxException e) {
            return null;
        }
    }
}
