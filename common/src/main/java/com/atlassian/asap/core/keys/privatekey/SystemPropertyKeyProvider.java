package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.core.keys.DataUriKeyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Reads private keys specified as data uri from system properties.
 */
public final class SystemPropertyKeyProvider extends DataUriKeyProvider {
    static final String URL_SCHEME = "sysprop";
    private static final Logger logger = LoggerFactory.getLogger(SystemPropertyKeyProvider.class);

    private SystemPropertyKeyProvider(String propertyName, DataUriKeyReader keyReader) {
        super(getDataUriFromSystemProperty(propertyName), keyReader);
    }

    private static URI getDataUriFromSystemProperty(String propertyName) {
        logger.debug("Reading private key from system property {}", propertyName);
        String systemPropertyValue = Optional.ofNullable(System.getProperty(propertyName))
                .orElseThrow(() -> new IllegalArgumentException("Undefined system property: " + propertyName));
        try {
            return new URI(systemPropertyValue);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Value of system property " + propertyName +
                    " cannot be parsed as a URI");
        }
    }

    /**
     * Instantiates the class by parsing a URL with the following syntax:
     * <code>sysprop:///[BASE_SYSTEM_PROPERTY_NAME]</code>.
     *
     * @param uri       URI with the syntax described above
     * @param keyReader reader of PEM documents
     * @return a new instance of the class
     */
    public static SystemPropertyKeyProvider fromUri(URI uri, DataUriKeyReader keyReader) {
        checkArgument(uri.isAbsolute(), "URL must be absolute"); // implies that scheme != null
        checkArgument(URL_SCHEME.equals(uri.getScheme()), "URL must have " + URL_SCHEME + " scheme");
        checkArgument(uri.getPath() != null && uri.getPath().startsWith("/"), "URL must have a path component");
        checkArgument(uri.getQuery() == null, "URL must not have a query component");
        checkArgument(uri.getAuthority() == null, "URL must not have an authority component");

        String basePropertyName = uri.getPath().substring(1); // remove the trailing slash
        return new SystemPropertyKeyProvider(basePropertyName, keyReader);
    }
}
