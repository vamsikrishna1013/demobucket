package com.atlassian.asap.nimbus.parser;

import com.atlassian.asap.api.JwsHeader.Header;
import com.atlassian.asap.api.JwtClaims.RegisteredClaim;
import com.atlassian.asap.core.SecurityProvider;
import com.atlassian.asap.core.exception.JwtParseException;
import com.atlassian.asap.core.exception.MissingRequiredClaimException;
import com.atlassian.asap.core.exception.MissingRequiredHeaderException;
import com.atlassian.asap.core.exception.UnsupportedAlgorithmException;
import com.atlassian.asap.core.parser.JwtParser;
import com.atlassian.asap.core.parser.VerifiableJwt;
import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONObject;

import java.security.Provider;
import java.text.ParseException;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

/**
 * A parser of JWT implemented using the Nimbus library.
 */
public class NimbusJwtParser implements JwtParser {

    private final Provider provider;

    public NimbusJwtParser() {
        this(SecurityProvider.getProvider());
    }

    public NimbusJwtParser(final Provider provider) {
        this.provider = provider;
    }

    @Override
    public VerifiableJwt parse(final String serializedJwt) throws JwtParseException, UnsupportedAlgorithmException {
        final JWSObject jwsObject;
        final JWTClaimsSet claims;

        try {
            jwsObject = JWSObject.parse(Objects.requireNonNull(serializedJwt));

            validateRequiredHeaders(jwsObject);

            JSONObject jsonPayload = Optional.ofNullable(jwsObject.getPayload()).map(Payload::toJSONObject)
                    .orElseThrow(() -> new JwtParseException("malformed payload"));

            claims = JWTClaimsSet.parse(jsonPayload);
        } catch (ParseException e) {
            throw new JwtParseException(e);
        }

        validateRequiredClaims(claims);
        try {
            return NimbusVerifiableJwt.buildVerifiableJwt(jwsObject, claims, provider);
        } catch (NumberFormatException ex) {
            throw new JwtParseException("unrepresentable JSON values", ex);
        }
    }

    @Override
    public Optional<String> determineUnverifiedIssuer(String serializedJwt) {
        try {
            JWSObject jwsObject = JWSObject.parse(Objects.requireNonNull(serializedJwt));
            Optional<JSONObject> jsonPayload = Optional.ofNullable(jwsObject.getPayload()).map(Payload::toJSONObject);
            if (jsonPayload.isPresent()) {
                JWTClaimsSet claims = JWTClaimsSet.parse(jsonPayload.get());
                return Optional.ofNullable(claims.getIssuer());
            } else {
                return Optional.empty();
            }
        } catch (ParseException e) {
            return Optional.empty();
        }
    }

    private void validateRequiredHeaders(JWSObject jwsObject) throws MissingRequiredHeaderException {
        if (jwsObject.getHeader().getAlgorithm() == null || jwsObject.getHeader().getAlgorithm() == Algorithm.NONE) {
            throw new MissingRequiredHeaderException(Header.ALGORITHM);
        }

        if (jwsObject.getHeader().getKeyID() == null) {
            throw new MissingRequiredHeaderException(Header.KEY_ID);
        }
    }

    private void validateRequiredClaims(JWTClaimsSet claims) throws MissingRequiredClaimException {
        checkClaimNotEmpty(claims.getAudience(), RegisteredClaim.AUDIENCE);
        checkClaimNotNull(claims.getIssuer(), RegisteredClaim.ISSUER);
        checkClaimNotNull(claims.getJWTID(), RegisteredClaim.JWT_ID);
        checkClaimNotNull(claims.getIssueTime(), RegisteredClaim.ISSUED_AT);
        checkClaimNotNull(claims.getExpirationTime(), RegisteredClaim.EXPIRY);
    }

    private static void checkClaimNotNull(Object claimValue, RegisteredClaim claim) throws MissingRequiredClaimException {
        if (claimValue == null) {
            throw new MissingRequiredClaimException(claim);
        }
    }

    private static void checkClaimNotEmpty(Collection claimValue, RegisteredClaim claim) throws MissingRequiredClaimException {
        if (claimValue == null || claimValue.isEmpty()) {
            throw new MissingRequiredClaimException(claim);
        }
    }
}
