package com.atlassian.asap.nimbus.parser;

import com.atlassian.asap.core.serializer.Json;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;

/**
 * Translates Nimbus (net.minidev.json) objects into JSR 353.
 */
class NimbusJsr353Translator {
    /**
     * Convert Nimbus JSON objects to JSR 353 JSON objects.
     *
     * @param nimbusObject a net.minidev.json compatible JSON object
     * @return a javax.json object
     * @throws NumberFormatException if values unrepresentable in JSON are present
     */
    static JsonValue nimbusToJsr353(Object nimbusObject) {
        if (nimbusObject instanceof JSONObject) {
            return convertObject((JSONObject) nimbusObject);
        } else if (nimbusObject instanceof JSONArray) {
            return convertArray((JSONArray) nimbusObject);
        } else if (nimbusObject instanceof Boolean) {
            return ((Boolean) nimbusObject) ? JsonValue.TRUE : JsonValue.FALSE;
        } else if (nimbusObject == null) {
            return JsonValue.NULL;
        } else if (nimbusObject instanceof String) {
            throw new RuntimeException("Cannot convert string values at the root level");
        } else if (nimbusObject instanceof Number) {
            throw new RuntimeException("Cannot convert numeric values at the root level");
        } else {
            throw new IllegalStateException("Broken JSON, type " + nimbusObject.getClass());
        }
    }

    private static JsonValue convertArray(JSONArray nimbusObject) {
        JsonArrayBuilder arrayBuilder = Json.provider().createArrayBuilder();
        for (Object arrayItem : nimbusObject) {
            if (arrayItem instanceof String) {
                arrayBuilder.add((String) arrayItem);
            } else if (arrayItem instanceof Integer) {
                arrayBuilder.add((Integer) arrayItem);
            } else if (arrayItem instanceof Long) {
                arrayBuilder.add((Long) arrayItem);
            } else if (arrayItem instanceof Double) {
                arrayBuilder.add((Double) arrayItem);
            } else if (arrayItem instanceof BigInteger) {
                arrayBuilder.add((BigInteger) arrayItem);
            } else if (arrayItem instanceof BigDecimal) {
                arrayBuilder.add((BigDecimal) arrayItem);
            } else { // all the other types can be converted recursively
                arrayBuilder.add(nimbusToJsr353(arrayItem));
            }
        }
        return arrayBuilder.build();
    }

    private static JsonValue convertObject(JSONObject nimbusObject) {
        JsonObjectBuilder objectBuilder = Json.provider().createObjectBuilder();
        for (Map.Entry<String, Object> entry : nimbusObject.entrySet()) {
            if (entry.getValue() instanceof String) {
                objectBuilder.add(entry.getKey(), (String) entry.getValue());
            } else if (entry.getValue() instanceof Integer) {
                objectBuilder.add(entry.getKey(), (Integer) entry.getValue());
            } else if (entry.getValue() instanceof Long) {
                objectBuilder.add(entry.getKey(), (Long) entry.getValue());
            } else if (entry.getValue() instanceof Double) {
                objectBuilder.add(entry.getKey(), (Double) entry.getValue());
            } else if (entry.getValue() instanceof BigInteger) {
                objectBuilder.add(entry.getKey(), (BigInteger) entry.getValue());
            } else if (entry.getValue() instanceof BigDecimal) {
                objectBuilder.add(entry.getKey(), (BigDecimal) entry.getValue());
            } else { // all the other types can be converted recursively
                objectBuilder.add(entry.getKey(), nimbusToJsr353(entry.getValue()));
            }
        }
        return objectBuilder.build();
    }
}
