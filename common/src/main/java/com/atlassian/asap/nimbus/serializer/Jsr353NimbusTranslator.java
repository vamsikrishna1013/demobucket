package com.atlassian.asap.nimbus.serializer;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import java.util.Map;

/**
 * Translates JSR 353 objects into the Nimbus (net.minidev.json) representation.
 */
class Jsr353NimbusTranslator {
    static Object jsr353ToNimbus(JsonValue jsonValue) {
        switch (jsonValue.getValueType()) {
            case NULL:
                return null;
            case STRING:
                return ((JsonString) jsonValue).getString();
            case NUMBER:
                return convertNumber((JsonNumber) jsonValue);
            case TRUE:
                return Boolean.TRUE;
            case FALSE:
                return Boolean.FALSE;
            case ARRAY:
                return convertArray((JsonArray) jsonValue);
            case OBJECT:
                return convertObject((JsonObject) jsonValue);
            default:
                throw new IllegalStateException("Broken JSON");
        }
    }

    private static Object convertNumber(JsonNumber jsonNumber) {
        // Do not use the ternary operator. Type inference gets horribly confused and always returns Double.
        if (jsonNumber.isIntegral()) {
            return jsonNumber.longValueExact();  // won't work for big integers
        } else {
            return jsonNumber.doubleValue();
        }
    }

    private static Object convertArray(JsonArray jsonValue) {
        JSONArray nimbusArray = new JSONArray();
        for (JsonValue arrayElement : jsonValue) {
            nimbusArray.add(jsr353ToNimbus(arrayElement));
        }
        return nimbusArray;
    }

    private static Object convertObject(JsonObject jsonValue) {
        JSONObject nimbusObject = new JSONObject();
        for (Map.Entry<String, JsonValue> entry : jsonValue.entrySet()) {
            nimbusObject.put(entry.getKey(), jsr353ToNimbus(entry.getValue()));
        }
        return nimbusObject;
    }
}
