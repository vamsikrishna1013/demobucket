package com.atlassian.asap.api.exception;

import com.atlassian.asap.core.validator.ValidatedKeyId;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.util.Optional;

/**
 * Thrown when a cryptographic key is not available.
 */
public class CannotRetrieveKeyException extends Exception {
    /**
     * The validated key ID used to try to retrieve the key. May be {@code null}
     */
    @Nullable
    private final transient ValidatedKeyId keyId;

    @Nullable
    private final URI keyUri;

    public CannotRetrieveKeyException(String reason) {
        this(reason, (URI) null);
    }

    public CannotRetrieveKeyException(String reason, URI keyUri) {
        this(reason, (ValidatedKeyId) null, keyUri);
    }

    public CannotRetrieveKeyException(String reason, ValidatedKeyId keyId) {
        this(reason, keyId, null);
    }

    public CannotRetrieveKeyException(String reason, ValidatedKeyId keyId, URI keyUri) {
        super(reason);
        this.keyId = keyId;
        this.keyUri = keyUri;
    }

    public CannotRetrieveKeyException(String message, Throwable cause) {
        this(message, cause, null);
    }

    public CannotRetrieveKeyException(String message, Throwable cause, URI keyUri) {
        this(message, cause, null, keyUri);
    }

    protected CannotRetrieveKeyException(String message, Throwable cause, ValidatedKeyId keyId, URI keyUri) {
        super(message, cause);
        this.keyId = keyId;
        this.keyUri = keyUri;
    }

    @Nonnull
    public final Optional<ValidatedKeyId> getKeyId() {
        return Optional.ofNullable(keyId);
    }

    @Nonnull
    public final Optional<URI> getKeyUri() {
        return Optional.ofNullable(keyUri);
    }
}
