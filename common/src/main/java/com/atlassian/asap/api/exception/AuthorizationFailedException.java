package com.atlassian.asap.api.exception;

/**
 * Thrown when an incoming HTTP request is authentic but is not authorized.
 */
public class AuthorizationFailedException extends Exception {
    public AuthorizationFailedException(String message) {
        super(message);
    }
}
