package com.atlassian.asap.api;

import javax.json.JsonObject;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;

/**
 * Represents the claims of an ASAP JWT payload.
 *
 * @see <a href="https://tools.ietf.org/html/rfc7519">JSON Web Token</a>
 */
public interface JwtClaims {
    /**
     * @return the value of the 'iss' claim. That is, the principal or application that issued the token.
     */
    String getIssuer();

    /**
     * @return the value of the 'sub' claim (optional). That is, the principal that a request is being made on the behalf of.
     */
    Optional<String> getSubject();

    /**
     * @return the value of the 'aud' claim. That is, the principal(s) or application(s) that the token is addressed to.
     */
    Set<String> getAudience();

    /**
     * @return the value of the 'exp' claim. That is, the expiration time after which the token MUST NOT be accepted for processing.
     */
    Instant getExpiry();

    /**
     * @return the value of the 'nbf' claim (optional). That is, the time before which the token MUST NOT be accepted for processing.
     */
    Optional<Instant> getNotBefore();

    /**
     * @return the value of the 'iat' claim. That is, the time at which the token was issued.
     */
    Instant getIssuedAt();

    /**
     * @return the value of the 'jti' claim. That is, the a unique identifier for the token.
     */
    String getJwtId();

    /**
     * @return all the claims, as a JSON object
     */
    JsonObject getJson();

    /**
     * The registered claims of a JWT claims payload as per JWT spec.
     */
    enum RegisteredClaim {
        ISSUER("iss"),
        SUBJECT("sub"),
        AUDIENCE("aud"),
        EXPIRY("exp"),
        NOT_BEFORE("nbf"),
        ISSUED_AT("iat"),
        JWT_ID("jti");

        private final String key;

        RegisteredClaim(String key) {
            this.key = key;
        }

        public String key() {
            return key;
        }
    }
}
