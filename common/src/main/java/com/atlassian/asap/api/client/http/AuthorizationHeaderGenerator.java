package com.atlassian.asap.api.client.http;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;

/**
 * HTTP clients can use this service to generate the value of the Authorization header to be included in
 * outgoing requests.
 *
 * @see <a href="http://s2sauth.bitbucket.org/">ASAP Authentication</a>
 */
public interface AuthorizationHeaderGenerator {
    /**
     * Generates an HTTP Authorization header that includes a signed JWT.
     *
     * @param jwt the JWT object to be included in the authorization header
     * @return the complete value of the Authorization header including the authentication scheme and the signed and serialized JWT
     * @throws InvalidTokenException      if the JWT is invalid or cannot be serialized
     * @throws CannotRetrieveKeyException if the private key is not available
     */
    String generateAuthorizationHeader(Jwt jwt) throws InvalidTokenException, CannotRetrieveKeyException;
}
