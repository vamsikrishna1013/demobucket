package com.atlassian.asap.api.exception;

/**
 * Thrown when an incoming HTTP request cannot be authenticated using ASAP, due to a permanent failure in the
 * incoming ASAP header (eg. missing header or failed claims)
 */
public class PermanentAuthenticationFailedException extends AuthenticationFailedException {
    public PermanentAuthenticationFailedException(String message) {
        this(message, null);
    }

    public PermanentAuthenticationFailedException(String message, String unverifiedIssuer) {
        super(message, unverifiedIssuer);
    }
}
