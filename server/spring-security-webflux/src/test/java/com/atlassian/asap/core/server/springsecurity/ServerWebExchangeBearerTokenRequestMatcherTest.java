package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.core.JwtConstants;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("ALL")//get ride of warnings like 'Method invocation 'isMatch' may produce 'java.lang.NullPointerException''
public class ServerWebExchangeBearerTokenRequestMatcherTest extends AbstractAsapAuthTest {

    @Test
    public void shouldNotMatchIfAuthHeaderNotPresent() {

        MockServerHttpRequest request = MockServerHttpRequest.get("/path").build();
        ServerWebExchangeMatcher.MatchResult matches = new ServerWebExchangeBearerTokenRequestMatcher().matches(createExchange(request)).block();

        assertFalse(matches.isMatch());
    }

    @Test
    public void shouldMatchIfAuthHeaderPresent() {

        MockServerHttpRequest request = MockServerHttpRequest
                .get("/path")
                .header(HttpHeaders.AUTHORIZATION, JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX + "abc-123")
                .build();
        ServerWebExchangeMatcher.MatchResult matches = new ServerWebExchangeBearerTokenRequestMatcher().matches(createExchange(request)).block();

        assertTrue(matches.isMatch());
    }

    @Test
    public void shouldNotMatchIfNotBearerAuth() {

        MockServerHttpRequest request = MockServerHttpRequest
                .get("/path")
                .header(HttpHeaders.AUTHORIZATION,  "dummy ")
                .build();
        ServerWebExchangeMatcher.MatchResult matches = new ServerWebExchangeBearerTokenRequestMatcher().matches(createExchange(request)).block();

        assertFalse(matches.isMatch());
    }
}