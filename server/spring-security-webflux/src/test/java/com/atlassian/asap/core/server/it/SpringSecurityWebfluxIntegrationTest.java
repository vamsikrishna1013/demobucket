package com.atlassian.asap.core.server.it;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.springsecurity.AsapAuthenticationWebFilter;
import com.atlassian.asap.core.server.springsecurity.AuthenticationProviderReactiveAuthenticationManager;
import com.atlassian.asap.core.server.springsecurity.IssuerAndSubjectWhitelistAsapAuthenticationProvider;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.JwtValidatorImpl;
import com.atlassian.asap.it.BaseIntegrationTest;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.JettyHttpHandlerAdapter;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.config.DelegatingWebFluxConfiguration;
import org.springframework.web.server.adapter.WebHttpHandlerBuilder;
import reactor.core.publisher.Mono;

import javax.servlet.Servlet;
import java.net.URI;
import java.util.Collections;
import java.util.List;

public class SpringSecurityWebfluxIntegrationTest extends BaseIntegrationTest {
    private static Server server;

    @Override
    protected URI getUrlForResourceName(String resourceName) {
        return URI.create("http://localhost:9000/" + resourceName);
    }

    @BeforeClass
    public static void startServer() throws Exception {

        server = new Server(9000);

        ApplicationContext context = new AnnotationConfigApplicationContext(DelegatingWebFluxConfiguration.class, SecurityConfig.class, ResourceController.class);
        HttpHandler handler = WebHttpHandlerBuilder.applicationContext(context).build();
        Servlet servlet = new JettyHttpHandlerAdapter(handler);

        ServletContextHandler contextHandler = new ServletContextHandler(server, "");
        contextHandler.addServlet(new ServletHolder(servlet), "/");

        server.start();
    }

    @AfterClass
    public static void stopServer() throws Exception {
        server.stop();
        server.destroy();
    }

    @Configuration
    @EnableWebFluxSecurity
    public static class SecurityConfig {

        static final String AUTHORITY = "VALID_SUBJECT";

        @Bean
        public JwtValidator jwtValidator() {
            return JwtValidatorImpl.createDefault(new AuthenticationContext(AUDIENCE, PUBLIC_KEY_PROVIDER));
        }

        @Bean
        public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity httpSecurity,
                                                             AsapAuthenticationWebFilter asapAuthenticationWebFilter,
                                                             ReactiveAuthenticationManager reactiveAuthenticationManager) {

            return httpSecurity
                    .addFilterAt(asapAuthenticationWebFilter, SecurityWebFiltersOrder.AUTHENTICATION)
                    .authenticationManager(reactiveAuthenticationManager)
                    .authorizeExchange()
                    .pathMatchers("/" + RESOURCE).hasAuthority(AUTHORITY)
                    .and()
                    .build();
        }

        @Bean
        public ReactiveAuthenticationManager reactiveAuthenticationManager(List<AuthenticationProvider> authenticationProviders) {
            return new AuthenticationProviderReactiveAuthenticationManager(authenticationProviders);
        }

        @Bean
        public AuthenticationProvider asapAuthenticationProvider(JwtValidator jwtValidator) {
            return new IssuerAndSubjectWhitelistAsapAuthenticationProvider(jwtValidator,
                    Collections.singleton(ISSUER1),
                    Collections.singleton(ISSUER1),
                    Collections.singleton(new SimpleGrantedAuthority(AUTHORITY)));
        }

        @Bean
        public AsapAuthenticationWebFilter asapAuthenticationWebFilter(ReactiveAuthenticationManager reactiveAuthenticationManager) {
            return new AsapAuthenticationWebFilter(reactiveAuthenticationManager);
        }
    }

    @RestController
    public static class ResourceController {
        @RequestMapping(RESOURCE)
        public Mono<String> serveResource() {
            return ReactiveSecurityContextHolder
                    .getContext()
                    .map(SecurityContext::getAuthentication)
                    .switchIfEmpty(Mono.empty())
                    .map(a -> ((Jwt) a.getCredentials()).getClaims().getJwtId());
        }
    }
}
