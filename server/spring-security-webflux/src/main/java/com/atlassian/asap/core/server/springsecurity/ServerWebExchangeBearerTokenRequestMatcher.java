package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.core.JwtConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * checks if {@code HttpHeaders.AUTHORIZATION} header of {@code ServerHttpRequest} contains Bearer token for further authentication.
 * Note: matcher does not validate actual value of the header somehow. It should be validated by the {@code ReactiveAuthenticationManager}
 * */
public class ServerWebExchangeBearerTokenRequestMatcher implements ServerWebExchangeMatcher {

    @Override
    public Mono<MatchResult> matches(ServerWebExchange exchange) {

        String authorizationHeader = exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        boolean match = StringUtils.isNotBlank(authorizationHeader) &&
                authorizationHeader.startsWith(JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX);

        if (!match) {
            return MatchResult.notMatch();
        }

        return MatchResult.match();
    }
}
