package com.atlassian.asap.core.server.jersey;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * Factory for {@link JerseyRequestAuthorizer}. Applications may override this factory to provide their own
 * authorizer.
 */
public class JerseyRequestAuthorizerFactory {
    /**
     * Instantiates {@link JerseyRequestAuthorizer} from the {@link JwtAuth} annotation.
     *
     * @param jwtAuth annotation in the Jersey resource. If it does not specify valid issuers, then the issuer
     *                whitelist is copied from the subject whitelist.
     * @return a new instance of the authorizer
     */
    public JerseyRequestAuthorizer create(JwtAuth jwtAuth) {
        Set<String> authorizedIssuers = ImmutableSet.copyOf(jwtAuth.authorizedIssuers());
        Set<String> authorizedSubjects = ImmutableSet.copyOf(jwtAuth.authorizedSubjects());
        return new WhitelistJerseyRequestAuthorizer(
                authorizedSubjects,
                authorizedIssuers.isEmpty() ? authorizedSubjects : authorizedIssuers);
    }
}
