package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import com.google.common.collect.ImmutableSet;
import com.sun.jersey.api.core.HttpRequestContext;

import java.util.Set;

/**
 * Decides if a request is authorized based on whitelists for the issuer and effective subject.
 */
public class WhitelistJerseyRequestAuthorizer implements JerseyRequestAuthorizer {
    private final Set<String> authorizedSubjects;
    private final Set<String> authorizedIssuers;

    public WhitelistJerseyRequestAuthorizer(Iterable<String> authorizedSubjects, Iterable<String> authorizedIssuers) {
        this.authorizedSubjects = ImmutableSet.copyOf(authorizedSubjects);
        this.authorizedIssuers = ImmutableSet.copyOf(authorizedIssuers);
    }

    @Override
    public void authorize(Jwt authenticJwt, HttpRequestContext requestContext) throws AuthorizationFailedException {
        if (!authorizedIssuers.contains(authenticJwt.getClaims().getIssuer())) {
            throw new AuthorizationFailedException("Issuer is not authorized");
        }

        String effectiveSubject = authenticJwt.getClaims().getSubject().orElse(authenticJwt.getClaims().getIssuer());
        if (!authorizedSubjects.contains(effectiveSubject)) {
            throw new AuthorizationFailedException("Effective subject is not authorized");
        }
    }
}
