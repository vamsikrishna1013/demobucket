package com.atlassian.asap.core.server.jersey;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface JwtAuth {
    /**
     * Only the subjects explicitly listed will be authorized.
     */
    String[] authorizedSubjects();

    /**
     * Only the issuers explicitly listed will be authorized. If omitted/empty, the list of authorized issuers will
     * be equal to the list of authorized subjects ({@link #authorizedSubjects()}.
     */
    String[] authorizedIssuers() default {};
}
