package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthenticationFailedException;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.sun.jersey.api.container.MappableContainerException;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.core.HttpRequestContext;
import com.sun.jersey.server.impl.inject.AbstractHttpContextInjectable;

import javax.ws.rs.core.HttpHeaders;
import java.util.Objects;

public class JwtInjectable extends AbstractHttpContextInjectable<Jwt> {
    private final RequestAuthenticator requestAuthenticator;
    private final JerseyRequestAuthorizer jerseyRequestAuthorizer;

    public JwtInjectable(RequestAuthenticator requestAuthenticator,
                         JerseyRequestAuthorizer jerseyRequestAuthorizer) {
        this.requestAuthenticator = Objects.requireNonNull(requestAuthenticator);
        this.jerseyRequestAuthorizer = Objects.requireNonNull(jerseyRequestAuthorizer);
    }

    @Override
    public Jwt getValue(HttpContext httpContext) {
        HttpRequestContext requestContext = httpContext.getRequest();
        String authorizationHeader = requestContext.getHeaderValue(HttpHeaders.AUTHORIZATION);

        try {
            // authenticate
            Jwt authenticJwt = requestAuthenticator.authenticateRequest(authorizationHeader);

            // authorize
            jerseyRequestAuthorizer.authorize(authenticJwt, requestContext);

            return authenticJwt;
        } catch (AuthenticationFailedException | AuthorizationFailedException e) {
            throw wrapException(e);
        }
    }

    private RuntimeException wrapException(Exception e) {
        // Non-jersey exceptions have to wrapped in a MappableContainerException if you want them to be handled
        // by a custom ExceptionMapper.
        // http://jersey.java.net/nonav/apidocs/1.12/jersey/com/sun/jersey/api/container/ContainerException.html
        return new MappableContainerException(e);
    }

    public RequestAuthenticator getRequestAuthenticator() {
        return this.requestAuthenticator;
    }

    public JerseyRequestAuthorizer getJerseyRequestAuthorizer() {
        return jerseyRequestAuthorizer;
    }
}
