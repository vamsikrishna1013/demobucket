package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.core.validator.JwtValidator;
import com.google.common.collect.ImmutableMap;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

/**
 * ASAP authentication provider that grants authorities based on a rule per issuer.
 *
 * @see AsapAuthenticationProvider base class documentation for more
 */
public class RulesAwareAsapAuthenticationProvider extends AsapAuthenticationProvider {
    private final Map<String, Function<Jwt, Collection<GrantedAuthority>>> issuerAuthorities;

    /**
     * @param jwtValidator      the validator of JWT tokens
     * @param issuerAuthorities the list of rules for each issuer
     */
    public RulesAwareAsapAuthenticationProvider(JwtValidator jwtValidator,
                                                Map<String, Function<Jwt, Collection<GrantedAuthority>>> issuerAuthorities) {
        super(jwtValidator);
        this.issuerAuthorities = ImmutableMap.copyOf(issuerAuthorities);
    }

    @Override
    protected Collection<GrantedAuthority> getGrantedAuthorities(Jwt validJwt) {
        return issuerAuthorities
                .getOrDefault(validJwt.getClaims().getIssuer(), (jwt) -> Collections.emptySet())
                .apply(validJwt);
    }

    // Helpers for constructing nice rules

    /**
     * Builds a function that checks that the issuer matches the effective subject and grants the given authorities.
     * @param grantedAuthorities the granted authorities
     * @return the function as described above
     */
    public static Function<Jwt, Collection<GrantedAuthority>> grantIfSubjectMatches(Collection<GrantedAuthority> grantedAuthorities) {
        return (Jwt jwt) -> effectiveSubject(jwt).equals(jwt.getClaims().getIssuer())
                ? grantedAuthorities
                : Collections.emptySet();
    }
}
