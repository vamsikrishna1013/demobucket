package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.core.JwtConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

/**
 * Checks that the request contains a bearer token.
 */
class BearerTokenRequestMatcher implements RequestMatcher {
    @Override
    public boolean matches(HttpServletRequest request) {
        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        return StringUtils.isNotBlank(authorizationHeader) &&
                authorizationHeader.startsWith(JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX);
    }
}
