package com.atlassian.asap.it;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.springsecurity.BearerTokenAuthenticationProcessingFilter;
import com.atlassian.asap.core.server.springsecurity.IssuerAndSubjectWhitelistAsapAuthenticationProvider;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.JwtValidatorImpl;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.DispatcherType;
import java.net.URI;
import java.util.Collections;
import java.util.EnumSet;

public class SpringSecurityIntegrationTest extends BaseIntegrationTest {
    private static Server server;

    @Override
    protected URI getUrlForResourceName(String resourceName) {
        return URI.create("http://localhost:9000/" + resourceName);
    }

    @BeforeClass
    public static void startServer() throws Exception {
        AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
        webApplicationContext.register(SecurityConfig.class, ResourceController.class);

        ServletContextHandler servletContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
        servletContext.addEventListener(new ContextLoaderListener(webApplicationContext));
        servletContext.addFilter(new FilterHolder(new DelegatingFilterProxy("springSecurityFilterChain")), "/*",
                EnumSet.allOf(DispatcherType.class));
        servletContext.addServlet(new ServletHolder(new DispatcherServlet(webApplicationContext)), "/*");

        server = new Server(9000);
        server.setHandler(servletContext);
        server.start();
    }

    @AfterClass
    public static void stopServer() throws Exception {
        server.stop();
        server.destroy();
    }

    @Configuration
    @EnableWebSecurity
    @EnableWebMvc
    public static class SecurityConfig extends WebSecurityConfigurerAdapter {
        @Bean
        public JwtValidator jwtValidator() {
            return JwtValidatorImpl.createDefault(new AuthenticationContext(AUDIENCE, PUBLIC_KEY_PROVIDER));
        }

        private AuthenticationEntryPoint entryPoint() {
            BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
            entryPoint.setRealmName("ASAP Realm");
            return entryPoint;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            BearerTokenAuthenticationProcessingFilter filter = new BearerTokenAuthenticationProcessingFilter(authenticationManager());
            http.addFilterBefore(filter, BasicAuthenticationFilter.class)
                    .authorizeRequests()
                    .antMatchers("/" + RESOURCE).hasAuthority("VALID_SUBJECT")
                    .and()
                    .exceptionHandling()
                    .accessDeniedHandler(new AccessDeniedHandlerImpl())
                    .authenticationEntryPoint(entryPoint());
        }

        @Override
        public void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(asapAuthenticationProvider(jwtValidator()));
        }

        @Bean
        public AuthenticationProvider asapAuthenticationProvider(JwtValidator jwtValidator) {
            return new IssuerAndSubjectWhitelistAsapAuthenticationProvider(jwtValidator,
                    Collections.singleton(ISSUER1),
                    Collections.singleton(ISSUER1),
                    Collections.singleton(new SimpleGrantedAuthority("VALID_SUBJECT")));
        }
    }

    @RestController
    public static class ResourceController {
        @RequestMapping(RESOURCE)
        public String serveResource() {
            Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getCredentials();
            return jwt.getClaims().getJwtId();
        }
    }
}
