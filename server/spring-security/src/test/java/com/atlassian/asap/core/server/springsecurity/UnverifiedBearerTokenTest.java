package com.atlassian.asap.core.server.springsecurity;

import org.junit.Test;
import org.springframework.security.core.Authentication;

import static org.springframework.util.Assert.notNull;

public class UnverifiedBearerTokenTest {
    @Test
    public void shouldAllowSetAuthenticatedFalse() { // as per the Javadoc
        new UnverifiedBearerToken("TOKEN").setAuthenticated(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectSetAuthenticatedTrue() {
        new UnverifiedBearerToken("TOKEN").setAuthenticated(true);
    }

    /**
     * Spring Security logs authentication failures and requires the Authentication object not to have a null name.
     */
    @Test
    public void mustHaveNonNullName() {
        Authentication auth = new UnverifiedBearerToken("monkey trousers");
        notNull(auth.getName());
    }
}
