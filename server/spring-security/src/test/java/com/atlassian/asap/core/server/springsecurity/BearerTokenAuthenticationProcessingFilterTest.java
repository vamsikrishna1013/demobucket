package com.atlassian.asap.core.server.springsecurity;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BearerTokenAuthenticationProcessingFilterTest {
    @Mock
    private AuthenticationManager authenticationManager;

    @InjectMocks
    private BearerTokenAuthenticationProcessingFilter filter;

    private MockHttpServletRequest request = new MockHttpServletRequest();
    private MockHttpServletResponse response = new MockHttpServletResponse();

    @Mock
    private FilterChain chain;

    @Mock
    private Authentication validAuthentication;

    @Captor
    private ArgumentCaptor<Authentication> authenticationCaptor;

    @Before
    @After
    public void clearContext() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void shouldSetAuthenticationInTheContextAndPropagateRequestIfTokenIsValid() throws Exception {
        request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer some-token");
        when(authenticationManager.authenticate(any(Authentication.class))).thenReturn(validAuthentication);

        filter.doFilter(request, response, chain);

        verify(authenticationManager).authenticate(authenticationCaptor.capture());
        assertThat(authenticationCaptor.getValue(), Matchers.instanceOf(UnverifiedBearerToken.class));
        assertThat(authenticationCaptor.getValue().getCredentials(), is("some-token"));
        verify(chain).doFilter(request, response);
        assertThat(SecurityContextHolder.getContext().getAuthentication(), is(validAuthentication));
    }

    @Test
    public void shouldRespondWith401AndNotPropagateRequestIfTokenIsInvalid() throws Exception {
        request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer some-token");
        when(authenticationManager.authenticate(any(Authentication.class)))
                .thenThrow(new AuthenticationException("") {
                });

        filter.doFilter(request, response, chain);

        verify(authenticationManager).authenticate(any(Authentication.class));
        verify(chain, never()).doFilter(request, response);
        assertNull(SecurityContextHolder.getContext().getAuthentication());
        assertThat(response.getStatus(), is(401));
    }

    @Test
    public void shouldNotAttemptAuthenticationAndPropagateRequestIfTokenIsAbsent() throws Exception {
        filter.doFilter(request, response, chain);

        verify(authenticationManager, never()).authenticate(any(Authentication.class));
        verify(chain).doFilter(request, response);
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }
}
