package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.core.exception.InvalidHeaderException;
import com.atlassian.asap.core.exception.JwtParseException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class AsapAuthenticationProviderTest {
    private static final String SERIALISED_TOKEN = "my-token";
    private static final Jwt JWT = JwtBuilder.newJwt().keyId("key-id").issuer("issuer").audience("audience").build();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Authentication authentication;

    @Mock
    private JwtValidator jwtValidator;

    @Mock
    private GrantedAuthority grantedAuthority;

    private ValidatedKeyId validatedKeyId;

    @Before
    public void prepareValidatedKey() throws InvalidHeaderException {
        validatedKeyId = ValidatedKeyId.validate("key-id");
    }

    @Before
    public void prepareAuthenticationContext() {
        when(authentication.getCredentials()).thenReturn(SERIALISED_TOKEN);
    }

    @Test(expected = BadCredentialsException.class)
    public void shouldFailIfTokenIsInvalid() throws Exception {
        when(jwtValidator.readAndValidate(SERIALISED_TOKEN))
                .thenThrow(new JwtParseException("token is invalid"));

        createSut().authenticate(authentication);
    }

    @Test(expected = AuthenticationServiceException.class)
    public void shouldFailIfRetrievingPublicKeyFails() throws Exception {

        when(jwtValidator.readAndValidate(SERIALISED_TOKEN))
                .thenThrow(new PublicKeyRetrievalException("key repo fail", validatedKeyId, null));

        createSut().authenticate(authentication);
    }

    @Test(expected = BadCredentialsException.class)
    public void shouldFailIfPublicKeyCannotBeFound() throws Exception {

        when(jwtValidator.readAndValidate(SERIALISED_TOKEN))
                .thenThrow(new PublicKeyNotFoundException("public key not found", validatedKeyId, null));

        createSut().authenticate(authentication);
    }

    @Test
    public void shouldReturnDefaultGrantedAuthoritiesAndSerializableJwtIfValidationSucceeds() throws Exception {
        when(jwtValidator.readAndValidate(SERIALISED_TOKEN)).thenReturn(JWT);

        Authentication authenticationResult = createSut().authenticate(authentication);
        assertEquals("issuer", authenticationResult.getPrincipal()); // token has no explicit subject
        assertEquals(JWT, authenticationResult.getCredentials());
        assertThat(authenticationResult.getCredentials(), instanceOf(Serializable.class));
        assertThat(authenticationResult.getAuthorities(), contains(grantedAuthority));
    }

    @Test(expected = InsufficientAuthenticationException.class)
    public void shouldFailIfPrincipalValidationFails() throws Exception {
        AsapAuthenticationProvider sut = new AsapAuthenticationProvider(jwtValidator) {
            @Override
            protected Collection<GrantedAuthority> getGrantedAuthorities(Jwt validJwt) throws AuthenticationException {
                throw new InsufficientAuthenticationException("no, you cannot authenticate");
            }
        };

        when(jwtValidator.readAndValidate(SERIALISED_TOKEN)).thenReturn(JWT);

        sut.authenticate(authentication);
    }

    @Test
    public void shouldAllowSubclassToModifyExtractedPrincipal() throws Exception {
        when(jwtValidator.readAndValidate(SERIALISED_TOKEN)).thenReturn(JWT);

        final AsapAuthenticationProvider authenticationProvider = new AsapAuthenticationProvider(jwtValidator) {
            @Override
            protected String extractPrincipal(Jwt jwt) {
                return "newPrincipal";
            }
        };
        Authentication authenticationResult = authenticationProvider.authenticate(authentication);
        assertEquals("newPrincipal", authenticationResult.getPrincipal()); // token has no explicit subject
    }

    @Test
    public void effectiveSubjectIsTheSubjectIfPresent() throws Exception {
        Jwt jwtWithSubject = JwtBuilder.copyJwt(JWT).subject(Optional.of("explicit-subject")).build();
        assertEquals("explicit-subject", AsapAuthenticationProvider.effectiveSubject(jwtWithSubject));
    }

    @Test
    public void effectiveSubjectIsTheIssuerIfSubjectNotPresent() throws Exception {
        Jwt jwtWithoutSubject = JwtBuilder.copyJwt(JWT).subject(Optional.<String>empty()).build();
        assertEquals(JWT.getClaims().getIssuer(), AsapAuthenticationProvider.effectiveSubject(jwtWithoutSubject));
    }

    private AsapAuthenticationProvider createSut() {
        return new AsapAuthenticationProvider(jwtValidator, Collections.singleton(grantedAuthority));
    }
}
