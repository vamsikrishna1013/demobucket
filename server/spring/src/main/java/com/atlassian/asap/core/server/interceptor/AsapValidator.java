package com.atlassian.asap.core.server.interceptor;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthorizationFailedException;

import java.util.Set;

import static com.atlassian.asap.core.server.interceptor.Memoizer.memoize;

@FunctionalInterface
public interface AsapValidator {
    /**
     * Validates a jwt token.
     *
     * @param asap The annotation instance on a method, class, or package
     * @param jwt  The jwt token
     * @throws AuthorizationFailedException If the validation fails for any reason
     */
    void validate(Asap asap, Jwt jwt) throws AuthorizationFailedException;

    /**
     * Composes two validators.
     *
     * @param then the validator to run after {@code this} one
     * @return a new validator
     */
    default AsapValidator andThen(AsapValidator then) {
        return (asap, jwt) -> {
            this.validate(asap, jwt);
            then.validate(asap, jwt);
        };
    }

    static AsapValidator newAnnotationValidator() {
        return new WhiteListAsapValidator(memoize(new WhiteListAsapValidator.AsapWhitelistProvider()));
    }

    static AsapValidator newEnvironmentVariablesValidator() {
        return new WhiteListAsapValidator(memoize(new WhiteListAsapValidator.EnvironmentVariablesWhitelistProvider()));
    }

    /**
     * This validator gives preferential treatment to the subjects and issuers present in the Asap annotation.
     * If either of them is not present in the asap annotation then it falls back to the supplied one.
     *
     * @param authorizedSubjects - Set of authorized subjects
     * @param authorizedIssuers  - Set of authorized Issuers
     * @return AsapValidator with annotation support with fallback to configuration
     */
    static AsapValidator newAnnotationWithConfigValidator(final Set<String> authorizedSubjects, final Set<String> authorizedIssuers) {
        return new WhiteListAsapValidator(new WhiteListAsapValidator.AsapAnnotationWhitelistProviderWithConfigSupport(authorizedSubjects, authorizedIssuers));
    }
}
