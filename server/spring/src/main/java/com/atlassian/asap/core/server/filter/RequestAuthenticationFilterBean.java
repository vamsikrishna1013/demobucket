package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.http.RequestAuthenticatorImpl;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.JwtValidatorImpl;

import javax.servlet.FilterConfig;
import java.util.Optional;

/**
 * Implements {@link AbstractRequestAuthenticationFilter} as a Java bean to make
 * it easier to wire this filter using Spring.
 */
public class RequestAuthenticationFilterBean extends AbstractRequestAuthenticationFilter {
    private final Optional<RequestAuthenticator> requestAuthenticator;

    /**
     * Using this constructor will try to automatically fetch your configuration from Spring.
     * You must have defined a bean of type {@link AuthenticationContext}.
     */
    public RequestAuthenticationFilterBean() {
        this(false);
    }

    public RequestAuthenticationFilterBean(boolean allowAnonymousRequests) {
        super(allowAnonymousRequests);
        this.requestAuthenticator = Optional.empty();
    }

    /**
     * Using this constructor you must specify your own already configured RequestAuthenticator.
     * Consider using one of the convenient {@link #createDefault} factory methods below.
     */
    public RequestAuthenticationFilterBean(RequestAuthenticator requestAuthenticator) {
        this(requestAuthenticator, false);
    }

    public RequestAuthenticationFilterBean(RequestAuthenticator requestAuthenticator, boolean allowAnonymousRequests) {
        super(allowAnonymousRequests);
        this.requestAuthenticator = Optional.of(requestAuthenticator);
    }

    /**
     * A factory to instantiate the filter with minimal configuration.
     *
     * @param audience             the audience this filter will accept requests for
     * @param publicKeyRepoBaseUrl the base URL of the public key repository
     * @return a web filter
     */
    public static RequestAuthenticationFilterBean createDefault(String audience, String publicKeyRepoBaseUrl) {
        AuthenticationContext authContext = new AuthenticationContext(audience, publicKeyRepoBaseUrl);
        JwtValidator jwtValidator = JwtValidatorImpl.createDefault(authContext);
        RequestAuthenticator requestAuthenticator = new RequestAuthenticatorImpl(jwtValidator);
        return new RequestAuthenticationFilterBean(requestAuthenticator);
    }

    @Override
    protected RequestAuthenticator getRequestAuthenticator(final FilterConfig filterConfig) {
        return requestAuthenticator.orElseGet(new SpringRequestAuthenticatorSupplier(filterConfig));
    }

}
