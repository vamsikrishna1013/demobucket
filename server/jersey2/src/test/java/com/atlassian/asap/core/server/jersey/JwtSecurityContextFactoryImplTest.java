package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.core.SecurityContext;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class JwtSecurityContextFactoryImplTest {
    private static final Jwt JWT_PROTOTYPE = JwtBuilder.newJwt().issuer("issuer").keyId("keyId").audience("audience").build();

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private SecurityContext previousSecurityContext;

    private JwtSecurityContextFactoryImpl jwtSecurityContextFactory = new JwtSecurityContextFactoryImpl();

    @Test
    public void shouldCopyTheSecureFlagFalseFromThePreviousSecurityContext() {
        when(previousSecurityContext.isSecure()).thenReturn(false);

        JwtSecurityContext securityContext = jwtSecurityContextFactory.createSecurityContext(JWT_PROTOTYPE, previousSecurityContext);

        assertThat(securityContext.isSecure(), is(false));
    }

    @Test
    public void shouldCopyTheSecureFlagTrueFromThePreviousSecurityContext() {
        when(previousSecurityContext.isSecure()).thenReturn(true);

        JwtSecurityContext securityContext = jwtSecurityContextFactory.createSecurityContext(JWT_PROTOTYPE, previousSecurityContext);

        assertThat(securityContext.isSecure(), is(true));
    }

    @Test
    public void shouldUseIssuerAsPrincipalWhenNoSubjectIsPresent() {
        JwtSecurityContext securityContext = jwtSecurityContextFactory.createSecurityContext(JWT_PROTOTYPE, previousSecurityContext);

        assertThat(securityContext.getUserPrincipal().getName(), is("issuer"));
    }

    @Test
    public void shouldUseSubjectAsPrincipalWhenSubjectIsPresent() {
        Jwt jwt = JwtBuilder.copyJwt(JWT_PROTOTYPE).subject(Optional.of("subject")).build();
        JwtSecurityContext securityContext = jwtSecurityContextFactory.createSecurityContext(jwt, previousSecurityContext);

        assertThat(securityContext.getUserPrincipal().getName(), is("subject"));
    }
}