package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.exception.AuthenticationFailedException;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import com.atlassian.asap.api.exception.PermanentAuthenticationFailedException;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Optional;

import static com.atlassian.asap.asap.AsapConstants.ASAP_REQUEST_ATTRIBUTE;
import static com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE;
import static com.google.common.collect.Sets.newHashSet;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AuthorizationRequestFilterTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private RequestAuthenticator authenticator;
    @Mock
    private ResourceInfo resourceInfo;
    @Mock
    private ContainerRequestContext context;
    @Mock
    private Jwt jwt;
    private AuthorizationRequestFilter filter;
    private AsapValidator asapValidator;

    @Before
    public void setUp() throws AuthenticationFailedException {
        String audience = "presence-test";
        JwtClaims claims = mock(JwtClaims.class);
        when(claims.getAudience()).thenReturn(newHashSet(audience));
        when(claims.getIssuer()).thenReturn("presence-test");
        when(claims.getSubject()).thenReturn(Optional.empty());
        when(jwt.getClaims()).thenReturn(claims);

        when(authenticator.authenticateRequest("Bearer validjwt")).thenReturn(jwt);

        when(authenticator.authenticateRequest("Bearer invalidjwt"))
                .thenThrow(new PermanentAuthenticationFailedException("invalid jwt", null));

        asapValidator = mock(AsapValidator.class);
        filter = new AuthorizationRequestFilter(new EmptyBodyFailureHandler(), asapValidator);
        filter.resourceInfo = resourceInfo;
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithAuthorizationFailure() throws IOException, NoSuchMethodException,
            AuthorizationFailedException {
        Asap asap = mock(Asap.class);
        when(context.getProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE)).thenReturn(jwt);
        when(context.getProperty(ASAP_REQUEST_ATTRIBUTE)).thenReturn(asap);

        doThrow(new AuthorizationFailedException("blah")).when(asapValidator).validate(any(Asap.class), eq(jwt));
        filter.filter(context);
        verify(context).abortWith(any(Response.class));
        verify(context).getProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithoutAsap() throws IOException, NoSuchMethodException {
        filter.filter(context);

        verify(context).getProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE);
        verify(context, never()).getProperty(ASAP_REQUEST_ATTRIBUTE);
    }
}
