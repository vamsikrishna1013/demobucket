package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.HashSet;
import java.util.Optional;
import java.util.function.Function;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class WhiteListAsapValidatorTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private Asap asap;

    @Mock
    private Jwt jwt;

    @Mock
    private JwtClaims claims;

    @Mock
    private Function<Asap, WhiteListAsapValidator.Whitelist> whitelistProvider;
    private HashSet<String> authorizedSubjects;
    private HashSet<String> authorizedIssuers;

    @Before
    public void setUp() {
        authorizedSubjects = newHashSet();
        authorizedIssuers = newHashSet();
        when(claims.getSubject()).thenReturn(Optional.empty());
        when(jwt.getClaims()).thenReturn(claims);
        when(whitelistProvider.apply(asap)).thenReturn(new WhiteListAsapValidator.Whitelist(
                authorizedSubjects, authorizedIssuers));

    }

    @Test
    public void validate() throws AuthorizationFailedException {
        authorizedIssuers.add("foo");
        when(claims.getIssuer()).thenReturn("foo");
        new WhiteListAsapValidator(whitelistProvider).validate(asap, jwt);
        verify(claims, times(2)).getIssuer();
    }

    @Test
    public void validateWithAllowAny() throws AuthorizationFailedException {
        when(claims.getIssuer()).thenReturn("foo");
        when(claims.getSubject()).thenReturn(Optional.of("bar"));
        new WhiteListAsapValidator(whitelistProvider).validate(asap, jwt);
        verify(claims, times(2)).getIssuer();
    }

    @Test
    public void validateWithEnvironmentVariable() throws AuthorizationFailedException {
        authorizedIssuers.add("foo");
        when(claims.getIssuer()).thenReturn("foo");
        new WhiteListAsapValidator(whitelistProvider).validate(asap, jwt);
        verify(claims, times(2)).getIssuer();
    }

    @Test
    public void validateWithMultiple() throws AuthorizationFailedException {
        authorizedIssuers.addAll(asList("fz", "foo"));
        when(claims.getIssuer()).thenReturn("foo");
        new WhiteListAsapValidator(whitelistProvider).validate(asap, jwt);
        verify(claims, times(2)).getIssuer();
    }

    @Test(expected = AuthorizationFailedException.class)
    public void validateFailedWithInvalidSubject() throws AuthorizationFailedException {
        authorizedIssuers.add("foo");
        authorizedSubjects.add("bar");
        when(claims.getIssuer()).thenReturn("foo");
        when(claims.getSubject()).thenReturn(Optional.of("baz"));
        new WhiteListAsapValidator(whitelistProvider).validate(asap, jwt);
    }

    @Test(expected = AuthorizationFailedException.class)
    public void validateFailedWithInvalidIssuer() throws AuthorizationFailedException {
        authorizedIssuers.add("foo");
        when(claims.getIssuer()).thenReturn("bar");
        new WhiteListAsapValidator(whitelistProvider).validate(asap, jwt);
    }

    @Test
    public void getWhitelistFromEnvironmentVariables() throws AuthorizationFailedException {
        WhiteListAsapValidator.Whitelist wl = new WhiteListAsapValidator.EnvironmentVariablesWhitelistProvider(
                "subjects", "issuers", ImmutableMap.of(
                "subjects", "foo,bar",
                "issuers", "baz"
        )).apply(asap);
        assertThat(wl.getAuthorizedSubjects(), equalTo(newHashSet("foo", "bar")));
        assertThat(wl.getAuthorizedIssuers(), equalTo(newHashSet("baz")));
    }

    @Test
    public void getWhitelistFromAnnotation() throws AuthorizationFailedException {
        when(asap.authorizedSubjects()).thenReturn(new String[]{"foo", "bar"});
        when(asap.authorizedIssuers()).thenReturn(new String[]{"baz"});
        WhiteListAsapValidator.Whitelist wl = new WhiteListAsapValidator.AsapWhitelistProvider().apply(asap);
        assertThat(wl.getAuthorizedSubjects(), equalTo(newHashSet("foo", "bar")));
        assertThat(wl.getAuthorizedIssuers(), equalTo(newHashSet("baz")));
    }

    @Test
    public void testWhiteListFromAnnotationWithConfigSupport() throws AuthorizationFailedException {
        when(asap.authorizedSubjects()).thenReturn(new String[]{"foo", "bar"});
        when(asap.authorizedIssuers()).thenReturn(new String[]{"baz"});
        WhiteListAsapValidator.Whitelist wl = getAnnotationProviderWithConfig().apply(asap);
        assertThat(wl.getAuthorizedSubjects(), equalTo(newHashSet("foo", "bar")));
        assertThat(wl.getAuthorizedIssuers(), equalTo(newHashSet("baz")));
    }

    @Test
    public void testWhiteListWhenAnnotationMissing() throws AuthorizationFailedException {
        when(asap.authorizedSubjects()).thenReturn(new String[]{});
        when(asap.authorizedIssuers()).thenReturn(new String[]{});
        WhiteListAsapValidator.Whitelist wl = getAnnotationProviderWithConfig().apply(asap);
        assertThat(wl.getAuthorizedSubjects(), equalTo(newHashSet("subject1", "subject2", "subject3")));
        assertThat(wl.getAuthorizedIssuers(), equalTo(newHashSet("issuer1", "issuer2", "issuer3")));
    }

    private Function<Asap, WhiteListAsapValidator.Whitelist> getAnnotationProviderWithConfig() {
        return new WhiteListAsapValidator.AsapAnnotationWhitelistProviderWithConfigSupport(ImmutableSet.of("subject1", "subject2", "subject3"), ImmutableSet.of("issuer1", "issuer2", "issuer3"));

    }


}
