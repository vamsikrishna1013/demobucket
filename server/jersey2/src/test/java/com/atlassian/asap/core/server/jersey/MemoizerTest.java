package com.atlassian.asap.core.server.jersey;

import org.junit.Test;

import java.util.List;
import java.util.function.Function;

import static com.atlassian.asap.core.server.jersey.Memoizer.memoize;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class MemoizerTest {
    @Test
    public void memoizeCalledOnce() {
        List<String> data = newArrayList();
        Function<String, Boolean> counter = memoize(data::add);

        assertThat(counter.apply("foo"), equalTo(true));
        assertThat(data.size(), equalTo(1));

        assertThat(counter.apply("foo"), equalTo(true));
        assertThat(data.size(), equalTo(1));
    }
}
