package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;

import org.apache.http.auth.BasicUserPrincipal;

import javax.ws.rs.core.SecurityContext;

import java.security.Principal;

/**
 * Implements {@link JwtSecurityContextFactory} building a {@link JwtSecurityContext} that has a basic principal
 * that is the effective subject of the {@link Jwt} token.
 */
public class JwtSecurityContextFactoryImpl implements JwtSecurityContextFactory {
    @Override
    public JwtSecurityContext createSecurityContext(Jwt jwt, SecurityContext previousSecurityContext) {
        return new JwtSecurityContextImpl(jwt, previousSecurityContext.isSecure());
    }

    private static class JwtSecurityContextImpl implements JwtSecurityContext {
        private final Jwt jwt;
        private final boolean isSecure;

        JwtSecurityContextImpl(Jwt jwt, boolean isSecure) {
            this.jwt = jwt;
            this.isSecure = isSecure;
        }

        @Override
        public Jwt getJwt() {
            return jwt;
        }

        @Override
        public Principal getUserPrincipal() {
            // We use the implementation of Principal from Apache HttpClient, since that is already a dependency
            // of ASAP. This is just for convenience, and we can replace it later.
            return new BasicUserPrincipal(jwt.getClaims().getSubject().orElse(jwt.getClaims().getIssuer()));
        }

        @Override
        public boolean isUserInRole(String role) {
            return false;  // not supported
        }

        @Override
        public boolean isSecure() {
            return isSecure;
        }

        @Override
        public String getAuthenticationScheme() {
            return BASIC_AUTH;
        }
    }
}
