package com.atlassian.asap.core.server.jersey;

/**
 * Handles authentication and authorization failures by sending back empty-bodied responses with the correct codes.
 */
@SuppressWarnings("WeakerAccess")
public class EmptyBodyFailureHandler implements FailureHandler {
}
