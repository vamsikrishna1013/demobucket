package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import static com.atlassian.asap.asap.AsapConstants.ASAP_REQUEST_ATTRIBUTE;
import static com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE;
import static com.atlassian.asap.core.server.jersey.AsapValidator.newAnnotationValidator;
import static java.util.Objects.requireNonNull;

/**
 * AuthorizationRequestFilter is a {@link ContainerRequestFilter} that validates an authenticated jwt token.
 */
@Provider
@Priority(Priorities.AUTHORIZATION)
public class AuthorizationRequestFilter implements ContainerRequestFilter {
    private static final Logger LOG = LoggerFactory.getLogger(AuthorizationRequestFilter.class);

    @SuppressWarnings("checkstyle:VisibilityModifier")
    @Context
    ResourceInfo resourceInfo;

    private final FailureHandler failureHandler;
    private final AsapValidator asapValidator;

    @SuppressWarnings("WeakerAccess")
    public AuthorizationRequestFilter(FailureHandler failureHandler, AsapValidator asapValidator) {
        this.asapValidator = requireNonNull(asapValidator);
        this.failureHandler = requireNonNull(failureHandler);
    }

    @Override
    public void filter(ContainerRequestContext context) {
        Jwt jwt = (Jwt) context.getProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE);
        if (jwt != null) {
            Asap asap = (Asap) context.getProperty(ASAP_REQUEST_ATTRIBUTE);
            authorizeToken(context, jwt, asap);
        }
    }

    private void authorizeToken(ContainerRequestContext context, final Jwt jwt, final Asap asap) {
        try {
            asapValidator.validate(asap, jwt);
            LOG.trace("Accepting authorized token with identifier '{}'", jwt.getClaims().getJwtId());
        } catch (AuthorizationFailedException e) {
            failureHandler.onAuthorizationFailure(context, e);
        }
    }

    /**
     * Use this factory method to create a new AuthorizationRequestFilter instance with the specified audience, issuers
     * list, and public key repository URL.
     *
     * @return the new instance
     */
    public static AuthorizationRequestFilter newInstance() {
        return new AuthorizationRequestFilter(new EmptyBodyFailureHandler(), newAnnotationValidator());
    }
}
