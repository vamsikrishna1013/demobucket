package com.atlassian.asap.core.server;

import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.publickey.PublicKeyProviderFactory;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import java.security.PublicKey;
import java.util.Set;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.ImmutableSet.copyOf;
import static java.util.Objects.requireNonNull;

/**
 * Provides the contextual information needed to authenticate a JWT.
 */
public class AuthenticationContext {
    private final Set<String> resourceServerAudiences;
    private final KeyProvider<PublicKey> publicKeyProvider;

    /**
     * Create a new {@link AuthenticationContext} encapsulating the required information to authenticate a JWT.
     *
     * @param resourceServerAudience the audience value of the resource server
     * @param publicKeyServerBaseUrl the base url of the public key server
     */
    public AuthenticationContext(@Nonnull String resourceServerAudience,
                                 @Nonnull String publicKeyServerBaseUrl) {
        this(ImmutableSet.of(requireNonNull(resourceServerAudience)), publicKeyServerBaseUrl);
    }

    /**
     * Create a new {@link AuthenticationContext} encapsulating the required information to authenticate a JWT.
     *
     * @param resourceServerAudiences the audience values of the resource server
     * @param publicKeyServerBaseUrl  the base url of the public key server
     */
    public AuthenticationContext(@Nonnull Set<String> resourceServerAudiences,
                                 @Nonnull String publicKeyServerBaseUrl) {
        this(resourceServerAudiences,
                PublicKeyProviderFactory.createDefault().createPublicKeyProvider(publicKeyServerBaseUrl));
    }

    /**
     * Create a new {@link AuthenticationContext} encapsulating the required information to authenticate a JWT.
     *
     * @param resourceServerAudience the audience value of the resource server
     * @param publicKeyProvider      the public key provider
     */
    public AuthenticationContext(@Nonnull String resourceServerAudience,
                                 @Nonnull KeyProvider<PublicKey> publicKeyProvider) {
        this(ImmutableSet.of(requireNonNull(resourceServerAudience)), publicKeyProvider);
    }

    /**
     * Create a new {@link AuthenticationContext} encapsulating the required information to authenticate a JWT.
     *
     * @param resourceServerAudiences the audience value of the resource server
     * @param publicKeyProvider       the public key provider
     */
    public AuthenticationContext(@Nonnull Set<String> resourceServerAudiences,
                                 @Nonnull KeyProvider<PublicKey> publicKeyProvider) {
        this.resourceServerAudiences = copyOf(requireNonNull(resourceServerAudiences));
        this.publicKeyProvider = requireNonNull(publicKeyProvider);
    }

    /**
     * @return the audience value of the resource server
     * @deprecated since 2.12, use {@link #getResourceServerAudiences()} instead
     */
    @Deprecated
    public String getResourceServerAudience() {
        checkState(resourceServerAudiences.size() == 1,
                "Legacy getResourceServerAudience can only be called if a single audience value has been set.");
        return Iterables.getFirst(resourceServerAudiences, null);
    }

    /**
     * @return the audience values of the resource server
     */
    public Set<String> getResourceServerAudiences() {
        return resourceServerAudiences;
    }

    /**
     * @return the public key provider
     */
    public KeyProvider<PublicKey> getPublicKeyProvider() {
        return publicKeyProvider;
    }
}
