package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.Jwt;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class AbstractRequestAuthorizationFilterTest {
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockFilterChain filterChain;
    private Jwt jwt;

    @Before
    public void createMocks() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        filterChain = new MockFilterChain();
        jwt = Mockito.mock(Jwt.class);
    }

    @After
    public void releaseMocks() {
        request = null;
        response = null;
        filterChain = null;
        jwt = null;
    }

    @Test(expected = IllegalStateException.class)
    public void shouldFailIfJwtTokenIsNotPresentInTheRequest() throws Exception {
        AbstractRequestAuthorizationFilter filter = createFilter(true);
        filter.doFilter(request, response, filterChain);
    }

    @Test
    public void shouldPropagateTheChainIfAuthorized() throws Exception {
        AbstractRequestAuthorizationFilter filter = createFilter(true);
        request.setAttribute(AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);

        filter.doFilter(request, response, filterChain);

        assertNotNull(filterChain.getRequest());
    }

    @Test
    public void shouldRejectIfUnauthorized() throws Exception {
        AbstractRequestAuthorizationFilter filter = createFilter(false);
        request.setAttribute(AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);

        filter.doFilter(request, response, filterChain);

        assertThat(response.getStatus(), equalTo(403));
        assertNull(filterChain.getRequest());
    }

    private static AbstractRequestAuthorizationFilter createFilter(boolean authorizationResult) {
        return new AbstractRequestAuthorizationFilter() {
            @Override
            protected boolean isAuthorized(HttpServletRequest request, Jwt jwt) {
                return authorizationResult;
            }
        };
    }
}
