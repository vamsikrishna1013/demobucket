package com.atlassian.asap.it;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter;
import com.atlassian.asap.core.server.filter.WhitelistRequestAuthorizationFilter;
import com.atlassian.asap.core.server.http.RequestAuthenticatorImpl;
import com.atlassian.asap.core.validator.JwtClaimsValidator;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.JwtValidatorImpl;
import com.atlassian.asap.nimbus.parser.NimbusJwtParser;
import com.google.common.collect.ImmutableSet;

import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.util.EnumSet;
import java.util.Set;

import static com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE;

public class ServletFilterIntegrationTest extends BaseIntegrationTest {
    private static final Set<String> AUTHORIZED_SUBJECTS = ImmutableSet.of(ISSUER1); // subject matches issuer
    private static final Set<String> AUTHORIZED_ISSUERS = ImmutableSet.of(ISSUER1);

    private static Server server;

    @Override
    protected URI getUrlForResourceName(String resourceName) {
        return URI.create("http://localhost:9000/" + resourceName);
    }

    @BeforeClass
    public static void startHttpServer() throws Exception {
        JwtValidator jwtValidator = new JwtValidatorImpl(PUBLIC_KEY_PROVIDER,
                new NimbusJwtParser(),
                new JwtClaimsValidator(Clock.systemUTC()),
                AUDIENCE);
        final RequestAuthenticator requestAuthenticator = new RequestAuthenticatorImpl(jwtValidator);

        Filter authenticationFilter = new AbstractRequestAuthenticationFilter() {
            @Override
            protected RequestAuthenticator getRequestAuthenticator(FilterConfig filterConfig) {
                return requestAuthenticator;
            }
        };
        Filter authorizationFilter = new WhitelistRequestAuthorizationFilter(AUTHORIZED_SUBJECTS, AUTHORIZED_ISSUERS);

        ServletContextHandler servletContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
        servletContext.addFilter(new FilterHolder(authenticationFilter), "/*",
                EnumSet.allOf(DispatcherType.class));
        servletContext.addFilter(new FilterHolder(authorizationFilter), "/*", EnumSet.allOf(DispatcherType.class));

        servletContext.addFilter(ResourceFilter.class, "/" + RESOURCE, EnumSet.allOf(DispatcherType.class));

        server = new Server(9000);
        server.setHandler(servletContext);
        server.start();
    }

    @AfterClass
    public static void stopHttpServer() throws Exception {
        server.stop();
    }

    public static class ResourceFilter implements Filter {
        @Override
        public void init(FilterConfig filterConfig) throws ServletException {
            // nothing
        }

        @Override
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                throws IOException, ServletException {
            Jwt jwt = (Jwt) request.getAttribute(AUTHENTIC_JWT_REQUEST_ATTRIBUTE);
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.setStatus(200);
            try (OutputStream out = httpServletResponse.getOutputStream()) {
                IOUtils.write(jwt.getClaims().getJwtId(), out, StandardCharsets.UTF_8);
            }
        }

        @Override
        public void destroy() {
            // nothing
        }
    }
}
