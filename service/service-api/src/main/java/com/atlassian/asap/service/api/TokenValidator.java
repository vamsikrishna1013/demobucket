package com.atlassian.asap.service.api;


import com.atlassian.asap.service.api.ValidationResult.Decision;

import java.util.Optional;

import static java.util.Arrays.asList;

/**
 * A builder-style tool for specifying and enforcing ASAP authentication and authorization requirements.
 *
 * @since 2.8
 */
public interface TokenValidator {
    /**
     * Specifies the issuer(s) that are authorized to use this resource. Issuers in this list would be able to execute
     * this resource without authorization checks.
     * Note, it's possible to list the same issuer both in 'issuer' and in 'subjectImpersonationIssuer'.
     * <p>
     * Issuer validation is appropriate unless the resource is intended to be used as an open service that many different issuers can access.
     * </p>
     * <p>
     * Failures reported as: {@link Decision#NOT_AUTHORIZED}
     * </p>
     *
     * @param authorizedIssuers the issuers that are authorized to use this resource; {@code null} elements are not permitted
     * @return {@code this}
     */
    default TokenValidator issuer(String... authorizedIssuers) {
        return issuer(asList(authorizedIssuers));
    }

    /**
     * As for {@link #issuer(String...)}.
     *
     * @param authorizedIssuers as for {@link #issuer(String...)}
     * @return {@code this}
     */
    TokenValidator issuer(Iterable<String> authorizedIssuers);

    /**
     * Specifies the issuer(s) that are authorized to impersonate a user while using this resource. Issuers in this list would be able to execute
     * this resource authorized as a certain user. Typically username comes as a subject in ASAP hence the name prefix.
     * <p>
     * Issuer validation is required to be used if the resource is intended to be used in the context of a user, with all the access checks
     * </p>
     * <p>
     * Failures reported as: {@link Decision#NOT_AUTHORIZED}
     * </p>
     *
     * @param impersonationIssuers the issuers that are authorized to impersonate a user while using this resource;
     *                             {@code null} elements are not permitted
     * @return {@code this}
     */
    default TokenValidator impersonationIssuer(String... impersonationIssuers) {
        return impersonationIssuer(asList(impersonationIssuers));
    }

    /**
     * As for {@link #impersonationIssuer(String...)}.
     *
     * @param impersonationIssuers as for {@link #impersonationIssuer(String...)}
     * @return {@code this}
     */
    TokenValidator impersonationIssuer(Iterable<String> impersonationIssuers);

    /**
     * Specifies that the subject is to be interpreted as identifying a specific user whose identity should be
     * assumed for this request.
     *
     * <p>
     * The subject is ignored by default.  If subject impersonation is enabled, then the subject is understood
     * to identify a user known to the application, or anonymous access when the subject is not specified.  If
     * subject impersonation is enabled, then the {@link #issuer(String...) issuer whitelist} <strong>MUST</strong>
     * be provided, or all tokens will be rejected.
     * </p>
     * <p>
     * The validation service only approves the request for subject impersonation.  The actual implementation is
     * left up to the surrounding framework.
     * </p>
     *
     * @param subjectImpersonation {@code true} to use subject impersonation
     * @return {@code this}
     * @deprecated move/copy issuers that are allowed to impersonate users from the 'issuer' to the 'impersonationIssuer' list
     */
    @Deprecated
    TokenValidator subjectImpersonation(boolean subjectImpersonation);

    /**
     * Specifies the effective subject(s) that are authorized to use this resource.
     * <p>
     * The subject is ignored by default.  If an explicit whitelist is provided, then the effective subject
     * must be in it for the token to pass authorization.  The effective subject is defined as:
     * </p>
     * <ul>
     * <li>The token's subject, if it is specified.</li>
     * <li>The token's issuer, if the subject is not specified and subject impersonation is not used.</li>
     * <li>Nobody at all, if the subject is not specified and subject impersonation is enabled.</li>
     * </ul>
     * <p>
     * Failures reported as: {@link Decision#NOT_AUTHORIZED}
     * </p>
     *
     * @param authorizedSubjects the subjects that are authorized to use this resource; {@code null} elements are not
     *                           permitted, and an empty list disables this check.
     * @return {@code this}
     */
    default TokenValidator subject(String... authorizedSubjects) {
        return subject(asList(authorizedSubjects));
    }

    /**
     * As for {@link #subject(String...)}.
     *
     * @param authorizedSubjects as for {@link #subject(String...)}
     * @return {@code this}
     */
    TokenValidator subject(Iterable<String> authorizedSubjects);

    /**
     * Specifies the audience(s) that will be accepted.
     *
     * <p>
     * By default, the globally configured audience is assumed to be the expected value, and it is always accepted.
     * If the token does not specify either the globally configured audience or one of the alternate values (if any)
     * provided here, then it will not pass authentication.
     * </p>
     * <p>
     * For security reasons, it is not possible to disable audience validation.  Note: This is an authentication
     * failure, because the token itself is not accepted as valid if it is meant for some other service.
     * </p>
     * <p>
     * Failures reported as: {@link Decision#NOT_AUTHENTICATED}
     * </p>
     *
     * @param additionalAudienceValues the audience values that, in addition to the globally configured default, are
     *                                 acceptable for other services to specify when creating a token to access this
     *                                 resource
     * @return {@code this}
     */
    default TokenValidator audience(String... additionalAudienceValues) {
        return audience(asList(additionalAudienceValues));
    }

    /**
     * As for {@link #audience(String...)}.
     *
     * @param additionalAudienceValues as for {@link #audience(String...)}
     * @return {@code this}
     */
    TokenValidator audience(Iterable<String> additionalAudienceValues);

    /**
     * Specifies the validation policy for this validator.
     *
     * <p>
     * By default, the validator will use {@link Policy#REQUIRE}.  If those are not the desired authentication
     * semantics, then this method can be used to specify one of the other {@link Policy policies}, instead.
     * The most useful ones are probably {@link Policy#IGNORE} (to disable ASAP authentication when it might
     * otherwise have been inherited from a superclass or the surrounding context) and {@link Policy#OPTIONAL},
     * to allow ASAP authentication to be validated when it is attempted, but without mandating its use.
     * </p>
     *
     * @param policy the enforcement policy to use
     * @return {@code this}
     */
    TokenValidator policy(Policy policy);

    /**
     * Perform the validation.
     *
     * @param authHeader the contents of the {@code Authorization} header from the HTTP request, if any.
     * @return the outcome from validating the {@code authHeader}
     */
    ValidationResult validate(Optional<String> authHeader);


    /**
     * Specifies the desired enforcement policy for ASAP authentication.
     */
    enum Policy {
        /**
         * Specifies that attempts to perform ASAP authentication with this resource should be actively rejected.
         *
         * <p>
         * This policy is intended to prohibit ASAP authentication when that ability might otherwise
         * have been inherited from some other source, such as a superclass or package annotation.
         * </p>
         * <p>
         * Note that since this policy refuses all ASAP tokens regardless of their contents, tokens are not passed
         * through their normal validity checks.  Any token received is assumed to pass authentication and
         * rejected as unauthorized, instead.
         * </p>
         */
        REJECT,

        /**
         * Specifies that attempts to perform ASAP authentication with this resource should be silently ignored.
         *
         * <p>
         * This annotation is intended to ignore ASAP authentication when that ability might otherwise
         * have been inherited from some other source, such as a superclass or package annotation.
         * The validator will always {@link Decision#ABSTAIN ABSTAIN} from the decision and
         * will not attempt to examine the {@code Authorization} header in any way.
         * </p>
         */
        IGNORE,

        /**
         * Specifies that ASAP authentication is optional, but will be enforced exactly as it would for the
         * {@link #REQUIRE} policy whenever a suitable {@code Authorization} header is present.
         *
         * <p>
         * This permits alternative forms of authentication and/or anonymous request processing to be used.
         * The resource is responsible for arranging whatever additional annotations and/or access filtering
         * might be needed.
         * </p>
         * <p>
         * Note that the only difference between {@code OPTIONAL} and {@code REQUIRED} is how the <em>complete
         * absence</em> of an ASAP token is handled, with this policy returning {@link Decision#ABSTAIN ABSTAIN}
         * instead of {@link Decision#NOT_AUTHENTICATED NOT_AUTHENTICATED} for that specific case.  In particular,
         * a client cannot attempt ASAP authentication, fail at it, and then proceed anonymously.  If an ASAP
         * token is provided, then it must meet all the necessary requirements, or the request will be rejected.
         * </p>
         */
        OPTIONAL,

        /**
         * Specifies that ASAP authentication is required.
         * <p>
         * If no authentication token is provided or if it does not satisfy the restrictions specified by
         * the annotation, then the request should be actively rejected.
         * </p>
         */
        REQUIRE
    }
}
