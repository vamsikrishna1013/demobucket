package com.atlassian.asap.service.api;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;

import javax.json.JsonObject;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;

/**
 * A builder-style tool for generating an HTTP {@code Authorization} header value that contains an ASAP token.
 *
 * @since 2.8
 */
public interface AuthorizationBuilder {
    /**
     * Specifies the subject of the token.
     *
     * @param subject the subject to specify, or {@code empty()} to omit this field from the token
     * @return {@code this}
     */
    AuthorizationBuilder subject(Optional<String> subject);

    /**
     * Specifies the intended audience(s) for the token.
     *
     * @param audience the audience(s) to specify in the token
     * @return {@code this}
     */
    default AuthorizationBuilder audience(String... audience) {
        return audience(Arrays.asList(audience));
    }

    /**
     * Specifies the intended audience(s) for the token.
     *
     * @param audience the audience(s) to specify in the token
     * @return {@code this}
     */
    AuthorizationBuilder audience(Iterable<String> audience);

    /**
     * Specifies how long the token will remain valid.
     *
     * @param duration how long the token should remain valid ({@code empty()} to use the default)
     * @return {@code this}
     */
    AuthorizationBuilder expiration(Optional<Duration> duration);

    /**
     * Permits the inclusion of additional parameters that are not covered by any of the standard JWT claims.
     * <p>
     * Other than the requirement that it must be expressible in JSON, no other explicit restrictions are given for the
     * contents of this field.  Practical restrictions, such as any maximum size for this information or what the
     * consequences might be for exceeding it, are left unspecified.
     * </p>
     *
     * @param claims the additional information, if any, to include as extended information in the token
     * @return {@code this}
     */
    AuthorizationBuilder customClaims(Optional<JsonObject> claims);

    /**
     * Specifies the time before which the token is considered invalid.
     *
     * @param notBefore defined the time before which the token is invalid, set it to Optional.empty() to apply no notBefore restrictions.
     * @return {@code this}
     */
    AuthorizationBuilder notBefore(Optional<Instant> notBefore);

    /**
     * Generates a signed token and forms a value that is appropriate to use as an "Authorization" HTTP header.
     *
     * @return the value that should be specified as an {@code Authorization} header in HTTP requests
     * @throws CannotRetrieveKeyException the private key cannot be loaded
     * @throws InvalidTokenException      the parameters supplied for the token were invalid
     */
    String build() throws InvalidTokenException, CannotRetrieveKeyException;
}
