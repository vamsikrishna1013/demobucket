package com.atlassian.asap.service.core.service;

import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.privatekey.NullKeyProvider;
import com.atlassian.asap.core.keys.privatekey.PrivateKeyProviderFactory;
import com.atlassian.asap.nimbus.serializer.NimbusJwtSerializer;
import com.atlassian.asap.service.core.spi.AsapConfiguration;

import java.net.URI;
import java.security.PrivateKey;

import static java.util.Objects.requireNonNull;

/**
 * An authorization header generator that is suitable for use as a globally configured component in a container.
 */
@SuppressWarnings("unused")
public class DefaultAuthorizationHeaderGenerator extends AuthorizationHeaderGeneratorImpl {
    /**
     * Constructs a new authorization header generator.
     *
     * @param config the configuration parameters for the ASAP services
     */
    public DefaultAuthorizationHeaderGenerator(AsapConfiguration config) {
        super(new NimbusJwtSerializer(), createPrivateKeyProvider(config));
    }

    private static KeyProvider<PrivateKey> createPrivateKeyProvider(AsapConfiguration config) {
        requireNonNull(config, "config");
        final String url = config.privateKeyUrl();
        return url.isEmpty() ? new NullKeyProvider() : PrivateKeyProviderFactory.createPrivateKeyProvider(URI.create(url));
    }
}
