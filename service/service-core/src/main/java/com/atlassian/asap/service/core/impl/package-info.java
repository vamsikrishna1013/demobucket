/**
 * Implementations for the token building and validation classes of the service API.
 */
@FieldsAreNonNullByDefault
@ParametersAreNonnullByDefault
@ReturnTypesAreNonNullByDefault

package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.annotation.FieldsAreNonNullByDefault;
import com.atlassian.asap.annotation.ReturnTypesAreNonNullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
