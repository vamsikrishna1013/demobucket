package com.atlassian.asap.service.core.impl;


import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.parser.JwtParser;
import com.atlassian.asap.core.validator.JwtClaimsValidator;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.JwtValidatorImpl;
import com.atlassian.asap.nimbus.parser.NimbusJwtParser;
import com.atlassian.asap.service.api.ValidationResult;
import com.atlassian.asap.service.core.spi.AsapConfiguration;
import com.google.common.annotations.VisibleForTesting;

import java.security.PublicKey;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.asap.core.JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX;
import static java.util.Objects.requireNonNull;

public class TokenValidatorImpl extends AbstractTokenValidator {
    private final KeyProvider<PublicKey> publicKeyProvider;
    private final JwtClaimsValidator jwtClaimsValidator;
    private final JwtParser jwtParser;

    public TokenValidatorImpl(AsapConfiguration config, KeyProvider<PublicKey> publicKeyProvider,
                              JwtClaimsValidator jwtClaimsValidator) {
        this(config, publicKeyProvider, jwtClaimsValidator, new NimbusJwtParser());
    }

    @VisibleForTesting
    TokenValidatorImpl(AsapConfiguration config, KeyProvider<PublicKey> publicKeyProvider,
                       JwtClaimsValidator jwtClaimsValidator, JwtParser jwtParser) {
        super(config);
        this.publicKeyProvider = requireNonNull(publicKeyProvider, "publicKeyProvider");
        this.jwtClaimsValidator = requireNonNull(jwtClaimsValidator, "jwtClaimsValidator");
        this.jwtParser = requireNonNull(jwtParser, "jwtParser");
    }

    @Override
    public ValidationResult validate(Optional<String> authHeader) {
        requireNonNull(authHeader, "authHeader");
        switch (policy()) {
            case REJECT:
                return rejectAsap(authHeader);
            case IGNORE:
                return ValidationResultImpl.abstain();
            case OPTIONAL:
                return optionalAsap(authHeader);
            case REQUIRE:
                return requireAsap(authHeader);
            default:
                throw new IllegalStateException("Unknown authorization policy: " + policy());
        }
    }

    private ValidationResult rejectAsap(Optional<String> authHeader) {
        return extractSerializedJwt(authHeader)
                .flatMap(jwtParser::determineUnverifiedIssuer)
                .map(ValidationResultImpl::rejected)
                .orElseGet(ValidationResultImpl::abstain);
    }

    private ValidationResult optionalAsap(Optional<String> authHeader) {
        return extractSerializedJwt(authHeader)
                .flatMap(this::parseAndVerifyToken)
                .orElseGet(ValidationResultImpl::abstain);
    }

    private ValidationResult requireAsap(Optional<String> authHeader) {
        return extractSerializedJwt(authHeader)
                .flatMap(this::parseAndVerifyToken)
                .orElseGet(ValidationResultImpl::notAuthenticated);
    }

    private Optional<ValidationResult> parseAndVerifyToken(String serializedJwt) {
        try {
            final JwtValidator jwtValidator = createJwtValidator(publicKeyProvider, jwtParser, jwtClaimsValidator,
                    acceptableAudienceValues());
            final Jwt jwt = jwtValidator.readAndValidate(serializedJwt);
            return Optional.of(verifyAuthorization(jwt));
        } catch (InvalidTokenException e) {
            // This exception doesn't tell us if the token was structurally invalid or if it made claims that
            // we cannot accept.  If we can pull an issuer out of it, then some kind of unacceptable JWT was
            // provided, so we should actively reject the request.  Otherwise, we can fall back on the default
            // handling for the request.
            return jwtParser.determineUnverifiedIssuer(serializedJwt)
                    .map(issuer -> ValidationResultImpl.notAuthenticated());
        } catch (CannotRetrieveKeyException e) {
            final String issuer = jwtParser.determineUnverifiedIssuer(serializedJwt).orElse(null);
            return Optional.of(ValidationResultImpl.notVerified(issuer));
        }
    }

    /**
     * Creates the validator that will enforce correctness of the token.
     *
     * @param publicKeyProvider  the source for resolving public keys
     * @param jwtParser          a parser for deserializing the JWT
     * @param jwtClaimsValidator the validator for enforcing authorization claims
     * @param allowedAudiences   the audience values that the token is allowed to specify for authenticating with us
     * @return a validator for the JWT
     */
    protected JwtValidator createJwtValidator(KeyProvider<PublicKey> publicKeyProvider, JwtParser jwtParser,
                                              JwtClaimsValidator jwtClaimsValidator, Set<String> allowedAudiences) {
        return new JwtValidatorImpl(publicKeyProvider, jwtParser, jwtClaimsValidator, allowedAudiences);
    }

    private ValidationResult verifyAuthorization(Jwt jwt) {
        if ((isImpersonationIssuerAuthorized(jwt) || isIssuerAuthorized(jwt)) && isSubjectAuthorized(jwt)) {
            return ValidationResultImpl.authorized(jwt);
        }

        return ValidationResultImpl.notAuthorized(jwt.getClaims().getIssuer());
    }

    private boolean subjectImpersonation(Jwt jwt) {
        return subjectImpersonation() || isImpersonationIssuerAuthorized(jwt);
    }

    private boolean isImpersonationIssuerAuthorized(Jwt jwt) {
        return impersonationAuthorizedIssuers().contains(jwt.getClaims().getIssuer());
    }

    //simplify as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
    private boolean isIssuerAuthorized(Jwt jwt) {
        if (authorizedIssuers().isEmpty()) {
            if (subjectImpersonation(jwt)) {
                throw new IllegalStateException("Subject impersonation requires an explicit issuer whitelist");
            }
            return true;
        }
        return authorizedIssuers().contains(jwt.getClaims().getIssuer());
    }

    private boolean isSubjectAuthorized(Jwt jwt) {
        final Set<String> subjects = authorizedSubjects();
        if (subjects.isEmpty()) {
            return true;
        }

        // The effective subject is the issuer if and only if the subject was unspecified and subject impersonation is not used.
        final String defaultSubject = subjectImpersonation(jwt) ? null : jwt.getClaims().getIssuer();
        final String effectiveSubject = jwt.getClaims().getSubject().orElse(defaultSubject);
        return subjects.contains(effectiveSubject);
    }

    private static Optional<String> extractSerializedJwt(Optional<String> authHeader) {
        return authHeader
                .filter(hdr -> hdr.startsWith(HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX))
                .map(hdr -> hdr.substring(HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX.length()));
    }
}
