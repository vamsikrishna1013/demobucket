package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.MissingRequiredClaimException;
import com.atlassian.asap.service.api.ValidationResult.Decision;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Optional;

import static com.atlassian.asap.service.api.TokenValidator.Policy.OPTIONAL;
import static com.atlassian.asap.service.api.ValidationResult.Decision.NOT_AUTHENTICATED;
import static com.atlassian.asap.service.api.ValidationResult.Decision.NOT_AUTHORIZED;
import static com.atlassian.asap.service.api.ValidationResult.Decision.NOT_VERIFIED;
import static java.util.Optional.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TokenValidatorImplOptionalPolicyTest extends AbstractTokenValidatorImplTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void noTokenReturnsAbstain() {
        tokenValidator.policy(OPTIONAL);

        assertThat(tokenValidator.validate(empty()), result(Decision.ABSTAIN));
    }

    @Test
    public void invalidTokenReturnsNotAuthenticated() throws Exception {
        tokenValidator.policy(OPTIONAL);
        when(jwtValidator.readAndValidate(TOKEN)).thenThrow(new MissingRequiredClaimException(JwtClaims.RegisteredClaim.ISSUER));

        assertThat(tokenValidator.validate(HEADER), result(NOT_AUTHENTICATED));
    }

    @Test
    public void unresolvableKeyReturnsNotVerified() throws Exception {
        tokenValidator.policy(OPTIONAL);
        when(jwtValidator.readAndValidate(TOKEN)).thenThrow(new CannotRetrieveKeyException("Ouch!"));

        assertThat(tokenValidator.validate(HEADER), result(NOT_VERIFIED, ISSUER));
    }

    @Test
    public void badIssuerReturnsNotAuthorized() throws Exception {
        tokenValidator.policy(OPTIONAL).issuer(AUDIENCE1);
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwtBuilder.build());

        assertThat(tokenValidator.validate(HEADER), result(NOT_AUTHORIZED, ISSUER));
    }

    @Test
    public void badEffectiveSubjectReturnsNotAuthorized() throws Exception {
        tokenValidator.policy(OPTIONAL).subject(AUDIENCE1);
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwtBuilder.build());

        assertThat(tokenValidator.validate(HEADER), result(Decision.NOT_AUTHORIZED, ISSUER));
    }

    @Test
    public void goodEffectiveSubjectReturnsAuthorized() throws Exception {
        tokenValidator.policy(OPTIONAL).subject(ISSUER);
        final Jwt jwt = jwtBuilder.build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    //remove as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
    public void subjectImpersonationRequiresAnIssuerWhitelist() throws Exception {
        tokenValidator.policy(OPTIONAL).subjectImpersonation(true);
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwtBuilder.build());

        thrown.expect(IllegalStateException.class);
        tokenValidator.validate(HEADER);
    }

    @Test
    //remove as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
    public void noEffectiveSubjectWhenImpersonationIsEnabled() throws Exception {
        tokenValidator.policy(OPTIONAL)
                .issuer(ISSUER)
                .subject(ISSUER)
                .subjectImpersonation(true);
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwtBuilder.build());

        assertThat(tokenValidator.validate(HEADER), result(Decision.NOT_AUTHORIZED, ISSUER));
    }

    @Test
    public void goodSubjectReturnsAuthorized() throws Exception {
        tokenValidator.policy(OPTIONAL).subject(ISSUER);
        final Jwt jwt = jwtBuilder.subject(Optional.of(ISSUER)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    //remove as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
    public void goodSubjectWithImpersonationReturnsAuthorized() throws Exception {
        tokenValidator.policy(OPTIONAL)
                .issuer(ISSUER)
                .subject(AUDIENCE1)
                .subjectImpersonation(true);
        final Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    public void validImpersonationReturnsAuthorized() throws Exception {
        tokenValidator.policy(OPTIONAL)
                .issuer(ISSUER2)
                .impersonationIssuer(ISSUER);
        Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    public void validImpersonationReturnsAuthorizedIssuerIsEmpty() throws Exception {
        tokenValidator.policy(OPTIONAL)
                .impersonationIssuer(ISSUER);
        Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    public void bothIssuerAndImpersonationIssuerAreEmpty() throws Exception {
        tokenValidator.policy(OPTIONAL);
        Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    public void invalidImpersonationReturnsUnauthorized() throws Exception {
        tokenValidator.policy(OPTIONAL)
                .issuer(ISSUER)
                .impersonationIssuer(ISSUER2);
        Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }
}
