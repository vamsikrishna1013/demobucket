package com.atlassian.asap.service.core.impl;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Optional;

import static com.atlassian.asap.service.api.TokenValidator.Policy.IGNORE;
import static com.atlassian.asap.service.api.ValidationResult.Decision.ABSTAIN;
import static java.util.Optional.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verifyZeroInteractions;

public class TokenValidatorImplIgnorePolicyTest extends AbstractTokenValidatorImplTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void alwaysIgnoresHeaderAndReturnsAbstain() {
        tokenValidator.policy(IGNORE);

        assertThat(tokenValidator.validate(empty()), result(ABSTAIN));
        assertThat(tokenValidator.validate(HEADER), result(ABSTAIN));
        assertThat(tokenValidator.validate(Optional.of("don't care")), result(ABSTAIN));

        verifyZeroInteractions(jwtParser, jwtValidator);
    }
}
