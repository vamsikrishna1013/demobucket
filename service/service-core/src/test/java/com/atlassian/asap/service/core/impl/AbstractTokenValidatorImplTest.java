package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.parser.JwtParser;
import com.atlassian.asap.core.validator.JwtClaimsValidator;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.service.api.ValidationResult;
import com.atlassian.asap.service.core.spi.AsapConfiguration;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.mockito.Mock;

import java.security.PublicKey;
import java.time.Clock;
import java.util.Optional;
import java.util.Set;

import static java.util.Optional.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@SuppressWarnings({"NullableProblems", "checkstyle:VisibilityModifier"})
abstract class AbstractTokenValidatorImplTest {
    static final String ISSUER = "harry";
    static final String ISSUER2 = "harry2";
    static final String AUDIENCE1 = "hogwarts";
    static final String TOKEN = "token";
    static final Optional<String> HEADER = Optional.of("Bearer token");

    private static final String KEY_ID = "harry/fawkes";

    @Mock
    AsapConfiguration config;
    @Mock
    KeyProvider<PublicKey> publicKeyProvider;
    @Mock
    JwtParser jwtParser;
    @Mock
    JwtValidator jwtValidator;

    JwtBuilder jwtBuilder = JwtBuilder.newJwt()
            .issuer(ISSUER)
            .keyId(KEY_ID)
            .audience(AUDIENCE1);
    Set<String> expectedAllowedAudiences = ImmutableSet.of(AUDIENCE1);
    TokenValidatorImpl tokenValidator;

    private JwtClaimsValidator jwtClaimsValidator;

    @Before
    public void setUp() {
        when(config.audience()).thenReturn(AUDIENCE1);
        when(jwtParser.determineUnverifiedIssuer(TOKEN)).thenReturn(Optional.of(ISSUER));

        this.jwtClaimsValidator = new JwtClaimsValidator(Clock.systemUTC());
        this.tokenValidator = new TokenValidatorFixture();
    }

    static Matcher<ValidationResult> result(ValidationResult.Decision decision) {
        return new ValidationResultMatcher(decision, empty(), empty());
    }

    static Matcher<ValidationResult> result(ValidationResult.Decision decision, Jwt jwt) {
        return new ValidationResultMatcher(decision, Optional.of(jwt), empty());
    }

    static Matcher<ValidationResult> result(ValidationResult.Decision decision, String untrustedIssuer) {
        return new ValidationResultMatcher(decision, empty(), Optional.of(untrustedIssuer));
    }

    private static final class ValidationResultMatcher extends TypeSafeMatcher<ValidationResult> {
        private final ValidationResult.Decision decision;
        private final Optional<Jwt> token;
        private final Optional<String> untrustedIssuer;

        private ValidationResultMatcher(ValidationResult.Decision decision, Optional<Jwt> token, Optional<String> untrustedIssuer) {
            this.decision = decision;
            this.token = token;
            this.untrustedIssuer = untrustedIssuer;
        }

        @Override
        protected boolean matchesSafely(ValidationResult validationResult) {
            return validationResult.decision().equals(decision)
                    && validationResult.token().equals(token)
                    && validationResult.untrustedIssuer().equals(untrustedIssuer);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText(MoreObjects.toStringHelper(ValidationResultImpl.class)
                    .add("decision", decision)
                    .add("token", token)
                    .add("untrustedIssuer", untrustedIssuer)
                    .toString());
        }
    }

    private class TokenValidatorFixture extends TokenValidatorImpl {
        TokenValidatorFixture() {
            super(AbstractTokenValidatorImplTest.this.config, publicKeyProvider, jwtClaimsValidator, jwtParser);
        }

        @Override
        protected JwtValidator createJwtValidator(KeyProvider<PublicKey> publicKeyProvider,
                                                  JwtParser jwtParser,
                                                  JwtClaimsValidator jwtClaimsValidator,
                                                  Set<String> allowedAudiences) {
            assertThat(allowedAudiences, is(expectedAllowedAudiences));
            return jwtValidator;
        }
    }
}
