package com.atlassian.asap.core.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
        "asap.issuer=issuer",
        "asap.key_id=keyId",
        "asap.private_key=data:application/pkcs8;kid=apikey;base64,MDoCAQAwDQYJKoZIhvcNAQEBBQAEJjAkAgEAAgMBGE4CAwEAAQICTGsCAwCMJwIBAgICTGsCAQACAkYU"
})
@ContextConfiguration(classes = {AsapClientConfigurationIntegrationTest.TestContext.class, AsapClientConfiguration.class})
public class AsapClientConfigurationIntegrationTest {
    @Autowired
    private AsapClientConfiguration asapClientConfiguration;

    @Test
    public void shouldGetIssuer() {
        assertThat(asapClientConfiguration.getIssuer(), is("issuer"));
    }

    @Test
    public void shouldGetKeyId() {
        assertThat(asapClientConfiguration.getKeyId(), is("keyId"));
    }

    static class TestContext {
        @Bean
        public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
            // required to expand the ${vars}
            return new PropertySourcesPlaceholderConfigurer();
        }
    }
}

