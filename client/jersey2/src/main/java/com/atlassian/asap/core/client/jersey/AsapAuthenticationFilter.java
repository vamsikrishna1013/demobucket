package com.atlassian.asap.core.client.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * Use this class to authenticate Jersey 2 client requests with ASAP.
 *
 * @since 2.10
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AsapAuthenticationFilter implements ClientRequestFilter {
    @VisibleForTesting
    static final String CLIENT_ASAP_TOKEN_ERROR_MESSAGE = "Client failed to generate ASAP token.";

    private static final Logger log = LoggerFactory.getLogger(AsapAuthenticationFilter.class);

    private final Jwt prototype;
    private final AuthorizationHeaderGenerator authorizationHeaderGenerator;

    public AsapAuthenticationFilter(Jwt prototype, URI privateKeyPath) {
        this(prototype, AuthorizationHeaderGeneratorImpl.createDefault(privateKeyPath));
    }

    public AsapAuthenticationFilter(Jwt prototype, AuthorizationHeaderGenerator authorizationHeaderGenerator) {
        this.prototype = Objects.requireNonNull(prototype);
        this.authorizationHeaderGenerator = Objects.requireNonNull(authorizationHeaderGenerator);
    }

    private String getAsapBearerToken() throws CannotRetrieveKeyException, InvalidTokenException {
        return authorizationHeaderGenerator.generateAuthorizationHeader(newJwtToken());
    }

    private Jwt newJwtToken() {
        Instant now = Instant.now();

        return JwtBuilder.copyJwt(prototype)
                .notBefore(Optional.of(now))
                .issuedAt(now)
                .expirationTime(now.plus(JwtBuilder.DEFAULT_LIFETIME))
                .jwtId(UUID.randomUUID().toString())
                .build();
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        if (!requestContext.getHeaders().containsKey(AUTHORIZATION)) {
            try {
                requestContext.getHeaders().add(AUTHORIZATION, getAsapBearerToken());
            } catch (CannotRetrieveKeyException | InvalidTokenException ex) {
                log.error("Exception generating ASAP token.", ex);
                requestContext.abortWith(Response.status(UNAUTHORIZED).entity(CLIENT_ASAP_TOKEN_ERROR_MESSAGE).build());
            }
        }
    }
}
