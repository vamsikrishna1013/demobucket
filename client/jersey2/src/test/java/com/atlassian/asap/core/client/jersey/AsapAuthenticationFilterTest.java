package com.atlassian.asap.core.client.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import org.glassfish.jersey.client.ClientRequest;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import static com.atlassian.asap.core.client.jersey.AsapAuthenticationFilter.CLIENT_ASAP_TOKEN_ERROR_MESSAGE;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AsapAuthenticationFilterTest {
    private static final String AUDIENCE = "my-audience";
    private static final String ISSUER = "my-issuer";
    private static final String KEY_ID = "my-key-id";
    private static final String AUTHORIZATION_HEADER_VALUE = "some-value";

    @Rule
    public MockitoRule mockitoJunitRule = MockitoJUnit.rule();

    @Mock
    private AuthorizationHeaderGenerator authorizationHeaderGenerator;
    @Mock
    private ClientRequest clientRequest;
    @Mock
    private MultivaluedMap<String, Object> headers;

    private AsapAuthenticationFilter asapAuthenticationFilter;

    @Before
    public void setup() {
        when(clientRequest.getHeaders()).thenReturn(headers);
        asapAuthenticationFilter = new AsapAuthenticationFilter(
                JwtBuilder.newJwt()
                        .audience(AUDIENCE)
                        .issuer(ISSUER)
                        .keyId(KEY_ID)
                        .build(),
                authorizationHeaderGenerator);
    }

    @Test
    public void shouldAddHeaderIfDoesNotExist() throws Exception {
        when(headers.containsKey(HttpHeaders.AUTHORIZATION)).thenReturn(false);
        when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenReturn(AUTHORIZATION_HEADER_VALUE);

        asapAuthenticationFilter.filter(clientRequest);

        verify(headers).add(eq(HttpHeaders.AUTHORIZATION), eq(AUTHORIZATION_HEADER_VALUE));
        verify(authorizationHeaderGenerator).generateAuthorizationHeader(any(Jwt.class));
    }

    @Test
    public void shouldNotAddHeaderIfItExists() throws Exception {
        when(headers.containsKey(HttpHeaders.AUTHORIZATION)).thenReturn(true);

        asapAuthenticationFilter.filter(clientRequest);

        verify(headers, never()).add(anyString(), anyString());
        verify(authorizationHeaderGenerator, never()).generateAuthorizationHeader(any(Jwt.class));
    }

    @Test
    public void shouldReturnUnauthorizedResponseIfTokenCanNotBeGenerated() throws Exception {
        when(headers.containsKey(HttpHeaders.AUTHORIZATION)).thenReturn(false);
        when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenThrow(new CannotRetrieveKeyException(""));

        asapAuthenticationFilter.filter(clientRequest);

        verify(headers, never()).add(anyString(), anyString());
        verify(clientRequest).abortWith(argThat(new UnauthorizedResponseMatcher()));
        verify(authorizationHeaderGenerator).generateAuthorizationHeader(any(Jwt.class));
    }

    private static class UnauthorizedResponseMatcher extends TypeSafeMatcher<Response> {
        @Override
        public void describeTo(Description description) {
            description.appendText("UNAUTHORIZED Response");
        }

        @Override
        protected boolean matchesSafely(Response response) {
            return UNAUTHORIZED.equals(Response.Status.fromStatusCode(response.getStatus())) &&
                    CLIENT_ASAP_TOKEN_ERROR_MESSAGE.equals(response.getEntity());
        }
    }
}
